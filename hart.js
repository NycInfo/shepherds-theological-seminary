var mysql = require('mysql');
var mongoose = require('mongoose');
var nodemailer = require('nodemailer');

var AppointmentSchema = mongoose.Schema({
	AppointmentId:"String",
  Name:"String",
  Email:"String",
  Mobile:"String",
  Address:"String",
  AppointmentDate:"String",
	AppointmentTime:"String",
  CreateDate:{type:Date, default:Date.now},
	UpdatedDate:{type:Date, default:Date.now},
	SubDomain:"String",
  Status:"String",
})
var StaffSchema = mongoose.Schema({
	StaffId:"String",
  Name:"String",
  Email:"String",
  Mobile:"String",
	Password:"String",
  Role:"String",
  CreateDate:{type:Date, default:Date.now},
	UpdatedDate:{type:Date, default:Date.now},
	SubDomain:"String",
  Status:"String",
})
var SiteRegSchema = mongoose.Schema({
	StudentId : "String",
	fname : "String",
	lname : "String",
	mobilenum :"String",
	courses : "String",
	Status : "String",
	CreateDate:{type:Date, default:Date.now},
	UpdatedDate:{type:Date, default:Date.now},
})
var contactusSchema = mongoose.Schema({
	Name : "String",
	Number : "String",
	Subject :"String",
	Message : "String",
	Status : "String",
	CreateDate:{type:Date, default:Date.now},
	UpdatedDate:{type:Date, default:Date.now},
})
var StudentSchema = mongoose.Schema({
	StudentId:"String",
	fname : "String",
	lname : "String",
	gender : "String",
	mobilenum : "String",
	password : "String",
	whatsappnumber : "String",
	landlinenumber : "String",
	dateofbirth : "String",
	RollNumber : "String",
	medium : "String",
	courses : "String",
	qualification : "String",
	dateofjoining : "String",
	coursefee : Number,
	paidfee : Number,
	address : "String",
	city : "String",
	Street : "String",
	Village : "String",
	state : "String",
	zipcode : "String",
	BooksSent : "String",
	CdSentStatus :  "String",
	Photo :   "String",
	Certificate :  "String",
	Status :  "String",
	optional1 : "String",
	optional2 : "String",
	optional3 : "String",
	CreateDate:{type:Date, default:Date.now},
	UpdatedDate:{type:Date, default:Date.now},
})
var appointments = mongoose.model('Appointments', AppointmentSchema);
var students = mongoose.model('Students', StudentSchema);
var registrations_site = mongoose.model('registrations_site', SiteRegSchema);
var contactuss = mongoose.model('contactus', contactusSchema);
var staff = mongoose.model('staff', StaffSchema);
module.exports = {
	port : process.env.PORT || 8081,

	url : 'mongodb://localhost:27017/shepherds',
	//url : 'mongodb://MongoAdmin:Dom_12#4@13.127.152.244:27017/niharmarket',
  mongo : mongoose,

	appointments:appointments,
	staff:staff,
	students:students,
	registrations_site:registrations_site,
	contactus:contactuss,
	bgPath : '/opt/Projects/shepherds-theological-seminary/Public3/images',
	transporter : nodemailer.createTransport({
		service: 'gmail',
		auth: {
			user: 'niharinfo321@gmail.com',
			pass: 'Nihar@321'
		}
	})
}
