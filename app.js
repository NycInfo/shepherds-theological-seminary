var http = require('http');
var fs = require('fs');
var url = require('url');
var path = require('path');
var express = require('express');
var app = express();
var session = require('express-session');
var MemoryStore = require('memorystore')(session);
var hart = require('./hart');
var bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

require('./routes/mainroute')(app);
require('./routes/AppointmentRoutes')(app);


hart.mongo.Promise = global.Promise;

hart.mongo.connect(hart.url,function(err,db){
  if(!err){
    console.log("mongodb connected successfully...");
  }else{
    console.log(err);
  }
});


app.use(express.static(path.join(__dirname, '')));

app.use(session({
secret: 'DoctorDx',
resave: true,
saveUninitialized: true,
cookie: { maxAge: 3000 },
store: new MemoryStore(),
maxAge: Date.now() + (30 * 86400 * 1000)
}));

app.get('/', function(req,res){

  delete req.session.Niharinfo;
  res.sendFile(path.resolve(__dirname, 'Public/signin.html'));
});
app.get('/logout', function(req,res){
  //req.session.destroy();
  delete req.session.DoctorDx;

  res.sendFile(path.resolve(__dirname, 'Public/signin.html'));
});

// app.get('*', function(req, res) {
//     res.redirect('https://' + req.headers.host + req.url);
// })
// Listen for requests
app.listen(hart.port,function(e){
   if(!e)
   {
      console.log('Server running at localhost:'+hart.port);
   }
})
