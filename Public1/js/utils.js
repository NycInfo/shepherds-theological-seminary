function checkInputPhNum(v,id){
	var d = v.trim();
	var pat=/^[0-9]+$/;
	if(d.charAt(1)==0 || !/^\(?(\d{3})\)?[- ]?(\d{3})[- ]?(\d{4})$/.test(d)){
		$('#'+id).val('').focus().notify("Enter correct phone no", { className: "error", position:"bottom", });
		return false;
	} else {
		$('.notifyjs-wrapper').trigger('notify-hide');
		return true;
	}

}
function IfscCode(v,id){
	var d=v.trim();
	var pat=/^[A-Za-z]{4}\d{7}$/;
	if(!email.match(pat) || Number(email) == 0 || email.charAt(0)==0 || email.length!=10){
		$('#'+id).val('').focus().notify("Enter correct zip", { className: "error", position:"bottom", });
		return false;
	} else {
		$('.notifyjs-wrapper').trigger('notify-hide');
		return true;
		}

}

// function chackInputAccountNumber(v,id){
// 	var d = v.trim();
// 	var pat=/^[0-10]+$/;
// 	if(d.charAt(1)==0 || !/^\(?(\d{3})\)?[- ]?(\d{3})[- ]?(\d{4})$/.test(d)){
// 		$('#'+id).val('').focus().notify("Enter correct phone no", { className: "error", position:"bottom", });
// 		return false;
// 	} else {
// 		$('.notifyjs-wrapper').trigger('notify-hide');
// 		return true;
// 	}
//
// }


function checkInputZip(v,id){
	var email = v.trim();
	var pat=/^[0-9]+$/;
	if(!email.match(pat) || Number(email) == 0 || email.charAt(0)==0 || email.length!=5){
		$('#'+id).val('').focus().notify("Enter correct zip", { className: "error", position:"bottom", });
		return false;
	} else {
		$('.notifyjs-wrapper').trigger('notify-hide');
		return true;
    }
}
function checkInputNum(v){
	var email = v.trim();
	var pat=/^[0-9]+$/;
	if(!email.match(pat) || Number(email) == 0 || email.charAt(0)==0){
		return false;
	} else {
		return true;
    }
}
function checkInputAlpha(v,id){
	var email = v.trim();
	var pat = /^[A-Za-z\s]+$/;
	if(email.match(pat)){
		$('.notifyjs-wrapper').trigger('notify-hide');
		return true;
	} else {
		$('#'+id).val('').focus().notify("Enter alphabets only", { className: "error", position:"bottom", });
		return false;
    }
}
function checkInputEmail(v,id){
	var email = v.toLowerCase().trim();
	var pat=/^([a-zA-Z])+([a-zA-Z0-9_\.\-])+\@([a-zA-Z])+(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9\-]{2,4})$/g
	//var pat=/^[a-zA-Z]+[A-Za-z0-9._-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,3}$/;
	if(email.match(pat)){
		$('.notifyjs-wrapper').trigger('notify-hide');
		return true;
	} else {
		$('#'+id).val('').focus().notify("Enter correct email", { className: "error", position:"bottom" });
		return false;
    }
}
