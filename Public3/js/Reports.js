
function GetDetails(){
var urldata = JSON.parse('{"' + decodeURI(location.search.substring(1).replace(/%2C/g, ",").replace(/&/g, "\",\"").replace(/%23/g, "#").replace(/=/g, "\":\"") + '"}'));



data = {};
data.id = urldata.aid;
$.ajax({
            data : data,
            dataType : 'json',
            type : 'POST',
            url : '/AgentProfileDetails1',
            success : function (response) {
              $("#FullName").html(response.UserName);
              $("#AgentName").html(response.UserName);
              $("#AgentEmail").html(response.email);
              $("#AgentMobile").html(response.Mobile);
            }
  });
  $.ajax({
              data : data,
              dataType : 'json',
              type : 'POST',
              url : '/AgentECommerceReports',
              success : function (response) {
                  var msg = '';
                  $.each(response,function(i,v){
                    msg += '<tr><td>'+v.BillNumber+'</td><td>'+v.ProductName+'</td><td>'+v.Amount+'</td><td>';
                    if(v.Status == 1){
                      msg += 'Not Delivered';
                    }
                    else{
                      msg += 'Delivered';
                    }
                    msg +='</td><td>'+moment(v.OrderDate).format('LLL')+'</td><td>'+v.Quantity+'</td><td>'+v.Quantity+'</td>'+v.SkuNumber+'</tr>';
                  })
                $("#agentecreports").html(msg);
              }

    });

    $.ajax({
                data : data,
                dataType : 'json',
                type : 'POST',
                url : '/AgentDMTReports',
                success : function (response) {
                    var msg = '';
                    $.each(response,function(i,v){
                      msg += '<tr><td>NHR123456</td><td>1500 Rs</td><td>AC123456</td><td>SBI / 23456</td><td>04-12-2017</td><td>Ramu</td></tr>';
                    })
                  $("#agentdmtreports").html(msg);
                }

      });

}

function agentsdata(){
  $.ajax({
      data       : "",
      dataType   : "json",
      type       : "POST",
      url        : "/getAgents",
      success    : function (data) {
      //  console.log(data);
        var mes = '';
          if(data.length>0){
               $.each(data,function(i,v){
                  mes += '<tr><td><a href="/AgentFullDetails?aid='+v._id+'">'+v.UserName+'</a></td><td>'+v.password+'</td><td>'+v.Mobile+'</td><td>'+v.Village+','+v.City+'</td><td>'+v.email+'</td><td><div class="row"><div class="col-sm-5"><a onclick = PanOutlet("'+v._id+'","'+v.UserName+'","'+v.Mobile+'","'+v.email+'","'+v.HNo+'","'+v.Street+'","'+v.Village+'","'+v.City+'","'+v.State+'","'+v.PinCode+'") >Create</a></div></td><td><div class="row"><div class="col-sm-5"><a onclick = Editagent("'+v._id+'") >Edit</a></div><div class="col-sm-7"><i class="fa fa-remove text-red fa-2x" onclick = deleteagent("'+v._id+'")></i></div></div></td></tr>';
               })
           } else {
                   mes += '<div class="active first_td">No data available...</div>';
               }
             $('#agentstable').html(mes);
             //$("#pr_table").DataTable();
      }

  });
}
$("#Add_Agent_Form").submit(function (e) {
    e.preventDefault();
    $('.notifyjs-wrapper').trigger('notify-hide');
    if(!checkInputAlpha($('#fname').val().trim()) || $('#fname').val().trim() == ''){
        $('#fname').focus().notify("Please Enter First Name...", { className: "error", position:"bottom" });
        return false;
    }
    if(!checkInputAlpha($('#lname').val().trim()) || $('#lname').val().trim() == ''){
        $('#lname').focus().notify("Please Enter Last Name...", { className: "error", position:"bottom" });
        return false;
    }
    if(!checkInputEmail($('#agent_email').val().trim())){
        $('#agent_email').focus().notify("Please Enter Valid Email...", { className: "error", position:"bottom" });
        return false;
    }
    if(!checkInputPhNum($('#mobilenum').val())){
        $('#mobilenum').focus().notify("Please Enter Mobile No...", { className: "error", position:"bottom" });
        return false;
    }
    if($('#address').val().trim() == ''){
        $('#address').focus().notify("Please Enter Address...", { className: "error", position:"bottom" });
        return false;
    }
    if(!checkInputAlpha($('#city').val().trim()) || $('#city').val().trim() == ''){
        $('#city').focus().notify("Please Enter City...", { className: "error", position:"bottom" });
        return false;
    }
    if($('#Street').val().trim() == ''){
        $('#Street').focus().notify("Please Enter Street Name...", { className: "error", position:"bottom" });
        return false;
    }
    if($('#Village').val().trim() == ''){
        $('#Village').focus().notify("Please Enter Village...", { className: "error", position:"bottom" });
        return false;
    }
    if(!checkInputAlpha($('#state').val().trim()) || $('#state').val().trim() == ''){
        $('#state').focus().notify("Please Enter State...", { className: "error", position:"bottom" });
        return false;
    }
    if(!checkInputNum($('#zipcode').val().trim()) || $('#zipcode').val().trim().length < 5){
        $('#zipcode').focus().notify("Please Enter Zip Code...", { className: "error", position:"bottom" });
        return false;
    }

    var formdata = $(this).serializeArray();
    var AgentId = $("#Agentid").val();
    if(AgentId == ""){
      var url = '/AddAgent';
    }
    else{
      var url = '/EditAgent';
    }
    $.ajax({
        url : url,
        type : 'POST',
        data : formdata,
        dataType : 'JSON',
        success : function(res){
          if(res == "0"){
            $('#addagentbtn').focus().notify("Email Already Exist...", { className: "error", position:"bottom" });
          }
          else{
            $('#addagentbtn').focus().notify("Agent Added Successfully", { className: "success", position:"bottom" });
            setTimeout(function(){
                $('#addagentmodal').modal('hide');
                agentsdata();
            }, 700);
          }
        }
    })
});
function deleteagent(id){
  if(confirm("Are you sure you want to delete this?")){
    data = {};
    data.aid = id;
    $.ajax({
                data : data,
                dataType : 'json',
                type : 'POST',
                url : '/DeleteAgent',
                success : function (response) {
                    console.log(response);
                }

      });
    }
    else{
        return false;
    }


}


function Editagent(id){
  $("#addagentmodal").modal("show");
  data = {};
  data.id = id;
  $.ajax({
        data : data,
        dataType : 'json',
        type : 'POST',
        url : '/AgentProfileDetails1',
        success : function (res) {
          $("#Agentid").val(id);
          $("#fname").val(res.UserName);
          $("#lname").val(res.UserName);
          $("#agent_email").val(res.email);
          $("#mobilenum").val(res.Mobile);
          $("#address").val(res.HNo);
          $("#city").val(res.City);
          $("#Street").val(res.Street);
          $("#Village").val(res.Village);
          $("#state").val(res.State);
          $("#zipcode").val(res.PinCode);
        }
    });
}

function PanOutlet(id,name,mobile,email,HNo,Street,Village,City,State,PinCode){
  $("#Agentidpan").val(id);
  $("#AgentId").val(id);
  $("#OutLetName").val(name);
  $("#MobileNumber").val(mobile);
  $("#OutLetMobileNumber").val(mobile);
  $("#OutLetEmail").val(email);
  $("#OutLetAddress").val(HNo+", "+Street+", "+Village+", "+City+", "+State);
  $("#OutLetPinCode").val(PinCode);
  $("#RegisterOutletModal").modal("show");
  return false;
}

$("#Outform1").submit(function(){
  // $("#Outletfirsttab").hide();
  // $("#Outlet1").show();
  // return false;
  var data = {};
  data.MobileNumber = $("#MobileNumber").val();
  if(data.MobileNumber == ""){
    $("#MobileNumber").notify("Enter Mobile Number", { className: "error", position:"right" });
    return false;
  }
  $.ajax({
      type: 'POST',
      data: JSON.stringify(data),
      contentType: 'application/json',
      url: '/VerifyOutlet',
      async:false,
      crossDomain:true,
      success: function(res) {
        console.log(res.statuscode);
        console.log(res.data.mobile_number);
        console.log(res.status);
        if(res.statuscode == "TXN"){
          $("#Outletfirsttab").hide();
          $("#Outlet1").show();
          return false;
        }
        else{
          $("#MobileNUmberSubmit").notify("Please Try Again..", { className: "error", position:"left" });
          return false;
        }
      }
  });
  return false;
})
