function GetNiharAdminReports(){
var urldata = JSON.parse('{"' + decodeURI(location.search.substring(1).replace(/%2C/g, ",").replace(/&/g, "\",\"").replace(/%23/g, "#").replace(/=/g, "\":\"") + '"}'));

data = {};
data.id = urldata.aid;
$('#example11').dataTable({
processing: true,
destroy:true,
autoWidth:false,
jQueryUI: true,
sAjaxDataProp: 'data',
ajax:{
url: "/GetRcReports_Nihar",
type: 'POST',
data: data,
dataSrc:'',
},
  columns: [
    {
          data: "orderid",
          className: "center",
          render: function(data, type, row, meta){
               data='<td><div class="row" style=" margin-left: -8px;"><a href="#" data-toggle="modal" data-target="#myModal11" onclick = ALLRechargeReports("'+row.orderid+'")>'+row.orderid+'</a></div></td>';
             return data;
          }
    },
    // { data : "operatorid" },
    { data : "MobileNumber" },

    { data : "servicekey"},
    { data : "TransactionAmount" },

    {
    data: "_id",
    className: "center",
    render: function(data, type, row, meta){
      //   if(type === 'display'){
           data='<td>'+moment(row.RechargeTime).format("DD MM YYYY HH:mm A")+'</td>';
    //   }
     return data;
    }
    },
    {
       data: "Status",
       className: "center",
       render: function(data, type, row, meta){
            // if(type === 'display'){
                 data='<td><div class="row"><div class="col-sm-5"><h6 style = "margin-top: -3px;">'+row.Status+'</h6><a  href="#" data-toggle="modal" data-target="#myModal11" onclick = NiharAdminReorts("'+row._id+'")>ViewDetails</a></div></td>';
          // }
         return data;

      }
   },

]
});
}

function NiharAdminReorts(id){

$.ajax({
  type: 'POST',
   data: {rechargeId:id},
   dataType:'json',
   url: '/GetAllRcReportsID',
    success: function(res) {
        if(res.Status == "SUCCESS"){
              walletID(res);
              $("#WalletNumber").show();
        }else if(res.Status == "Fail"){
           $("#WalletNumber").hide();
        }
      $('#myModal11').modal('show');
      console.log("Helooe");
      $("#OperatorId").html(res.orderid);
      $("#MobileNumber").html(res.MobileNumber);
      $("#ChargedAmount").html(res.TransactionAmount);
      $("#ResponseMessage").html(res.responsemsg);
      var data = moment(res.RechargeTime).format("DD MMM  YYYY, h:mm a");
        $("#ResponseTime").html(data);
    }
});
}
function ALLRechargeReports(order){
$.ajax({
  type: 'POST',
   data: {orderid:order},
   dataType:'json',
   url: '/GetAllOrderReports',
    success: function(res) {

    if(res.Status == "SUCCESS"){
          walletID(res);
          $("#WalletNumber").show();
    }else if(res.Status == "Fail"){
       $("#WalletNumber").hide();
    }
      $("#myModal11").modal('show');
      $("#OperatorId").html(res.orderid);
      $("#MobileNumber").html(res.MobileNumber);
      $("#ChargedAmount").html(res.TransactionAmount);
      $("#ResponseMessage").html(res.responsemsg);
      var data = moment(res.RechargeTime).format("DD MMM  YY, h:mm a");
        $("#ResponseTime").html(data);
        $("#myModal11").hide();


    }
});
}
function walletID(res){
  //alert("iii");
  $("#TransactionId").val(res.orderid);
  $("#ServicessName").val(res.servicekey)
  $("#myModalwallet").hide();
}
$("#sendEmail").click(function(){
  var data = {};
  data.ToAddress = $("#mailtoagent").val();
  data.Subject = $("#subjectmail").val();
  data.ServiceName = $("#ServicessName").val();
  data.TransactionId = $("#TransactionId").val();
  data.Email_Body = $('#bodymail').val();
  console.log(data);
  if(data.ToAddress == ""){
      $("#mailtoagent").notify("Please Enter AgentEmail..", { className: "error", position:"top" });
      return false;
  }
  if(data.Subject == ""){
      $("#subjectmail").notify("Please fill this field", { className: "error", position:"top" });
      return false;
  }
console.log(data);
//return false;
  $.ajax({
      type: 'POST',
      data: JSON.stringify(data),
      contentType: 'application/json',
      url: '/Sendmailtocustomerservices',
      async:true,
      crossDomain:true,
      success: function(res) {
        if(res == "1"){
          $("#sendEmail").notify("Message Sent Successfully..", { className: "success", position:"left" });
          setTimeout(function() {
           location.reload();
         },3000);
          // $("#subjectmail").val("");
          // $('#bodymail').val("");
          return false;
        }
        else{
          $("#sendEmail").notify("Message Not Sent..", { className: "error", position:"left" });
           $('#sendemailform')[0].reset();
          return false;
        }
      }
  });
});

function GetNiharAgentBusBookingReports(){
var urldata = JSON.parse('{"' + decodeURI(location.search.substring(1).replace(/%2C/g, ",").replace(/&/g, "\",\"").replace(/%23/g, "#").replace(/=/g, "\":\"") + '"}'));

data = {};
data.id = urldata.aid;
$('#example3').dataTable({
processing: true,
destroy:true,
autoWidth:false,
jQueryUI: true,
sAjaxDataProp: 'data',
ajax:{
url: "/GetNiharAgentBusBookingReports",
type: 'POST',
data: data,
dataSrc: ""
},
columns: [
   {
         data: "BookingID",
         className: "center",
         render: function(data, type, row, meta){
         //   if(type === 'display'){
              data='<td><div class="row" style=" margin-left: -7px;"><a href="#" data-toggle="modal" data-target="#BusBookingReports" onclick = AllBusBooking("'+row.BookingID+'")>'+row.BookingID+'</a></div></td>';
         //   }
            return data;
         }
      },
 //{ data : "TransactionID" },
     { data : "Source" },
     { data : "Destination" },
     { data : "Fare" },
     {
    data: "bookedAt",
    className: "center",
    render: function(data, type, row, meta){
          //if(type === 'display'){

            data='<td>'+moment(row.bookedAt).format("D MMM YYYY HH:mm A")+'</td>';
       // }
      return data;
    }
    },
    {
       data: "Date_jou",
       className: "center",
       render: function(data, type, row, meta){
           //  if(type === 'display'){

               data='<td>'+moment(row.Date_jou).format("D MMM YYYY HH:mm A")+'</td>';
       //  }
         return data;
      }
     },
 {data  :"Passenger"},
 {
     data: "status",
     className: "center",
     render: function(data, type, row, meta){
         //  if(type === 'display'){
               data='<td><div class="row"><div class="col-sm-5"><h6 style = "margin-top: -3px;">'+row.status+'</h6><a href="#" data-toggle="modal" data-target="#BusBookingReports" onclick = BusBooking("'+row._id+'")>ViewDetails</a></div></td>';
       //  }
       return data;
     }
 },

 ]
} );
}

function BusBooking(id){
$.ajax({
  method : 'POST',
  url    : '/GetBusBookingID',
  data   : {BusBookingId:id},
  dataType :'json',
  success : function(res){
    console.log(res.bookingstatus);
    if(res.status == "1"){
          walletID(res);
          $("#WalletNumber").show();
    }else if(res.status == "0"){
       $("#WalletNumber").hide();
    }
    console.log(res);
    $("#BusBookingReports").modal('show');
    $('#TransactionID').html(res.BookingID);
    $('#Source').html(res.Source);
    $('#Destination').html(res.Destination);
    $('#Fair').html(res.Fare);
    $('#OperatorName').html(res.Passenger);
    //$('#ResponseMessage').html(res.bookingstatus);
    var DateOfBooking = moment(res.bookedAt).format('DD MMM YYYY h:mm A');
    var DateOfJourney = moment(res.Date_jou).format("DD MMM  YYYY, h:mm A");
    $('#DateOfBooking').html(DateOfBooking)
    $('#DateOfJourney').html(DateOfJourney)
  }
});
}

function AllBusBooking(transId){
  alert(transId)
$.ajax({
  method : 'POST',
  url    : '/GetOrderIdReports',
  data   : {TransactionID:transId},
  dataType :'json',
  success : function(res){
    console.log(res.bookingstatus);
    if(res.status == "1"){
          walletID(res);
          $("#WalletNumber").show();
    }else if(res.status == "0"){
       $("#WalletNumber").hide();
    }
    $("#BusBookingReports").modal('show');
    $('#TransactionID').html(res.BookingID);
    $('#Source').html(res.Source);
    $('#Destination').html(res.Destination);
    $('#Fair').html(res.Fare);
    $('#OperatorName').html(res.Passenger);
  //  $('#ResponseMessage').html(res.bookingstatus);

    var DateOfBooking = moment(res.bookedAt).format('DD MMM YY h:mm a');
    var DateOfJourney = moment(res.Date_jou).format("DD MMM  YY, h:mm a");
    $('#DateOfBooking').html(DateOfBooking)
    $('#DateOfJourney').html(DateOfJourney)
    $("#BusBookingReports").hide();

  }
});
}
function walletID(res){
 //alert("iii");
 $("#TransactionId").val(res.BookingID);
 $("#ServicessName").val('BusBooking');
 $("#myModalwallet").modal('hide');
}
$("#sendEmail").click(function(){
 var data = {};
 data.ToAddress = $("#mailtoagent").val();
 data.Subject = $("#subjectmail").val();
 data.ServiceName = $("#ServicessName").val();
 data.TransactionId = $("#TransactionId").val();
 data.Email_Body = $('#bodymail').val();
 console.log(data);
 if(data.ToAddress == ""){
     $("#mailtoagent").notify("Please Enter AgentEmail..", { className: "error", position:"top" });
     return false;
 }
 if(data.Subject == ""){
     $("#subjectmail").notify("Please fill this field", { className: "error", position:"top" });
     return false;
 }
console.log(data);
//return false;
 $.ajax({
     type: 'POST',
     data: JSON.stringify(data),
     contentType: 'application/json',
     url: '/Sendmailtocustomerservices',
     async:true,
     crossDomain:true,
     success: function(res) {
       if(res == "1"){
         $("#sendEmail").notify("Message Sent Successfully..", { className: "success", position:"left" });
         setTimeout(function() {
          location.reload();
        },3000);
         // $("#subjectmail").val("");
         // $('#bodymail').val("");
         return false;
       }
       else{
         $("#sendEmail").notify("Message Not Sent..", { className: "error", position:"left" });
          $('#sendemailform')[0].reset();
         return false;
       }
     }
 });
})

function GetNiharAgentDirect_Money_Transfer_Reportss(){
var urldata = JSON.parse('{"' + decodeURI(location.search.substring(1).replace(/%2C/g, ",").replace(/&/g, "\",\"").replace(/%23/g, "#").replace(/=/g, "\":\"") + '"}'));

data = {};
data.id = urldata.aid;

$('#example4').DataTable({
processing: true,
destroy:true,
autoWidth:false,
jQueryUI: true,
sAjaxDataProp: 'data',
ajax:{
url: "/GetNiharAgentDMTReports",
type: 'POST',
data: data,
dataSrc: ""
},
columns: [
  {
        data: "ReferenceNumber",
        className: "center",
        render: function(data, type, row, meta){
          console.log(row);
       //    if(type === 'display'){
             data='<td><div class="row" style=" margin-left: -7px;"><a href="#" data-toggle="modal" data-target="#DMTTransfersReports" onclick = AllRefernceNumberId("'+row.ReferenceNumber+'")>'+row.ReferenceNumber+'</a></div></td>';
       //    }
           return data;
        }
     },
{ data : "Name" },
{ data : "AccountNumber" },
{ data : "Bank" },
{ data : "Branch"},
{ data : "Ifsc" },
{ data : "Amount"},
{
data: "CreateDate",
className: "center",
render: function(data, type, row, meta){
     // if(type === 'display'){

        data='<td>'+moment(row.CreateDate).format("DD MMM  YYYY, h:mm A")+'</td>';
   // }
  return data;
}
},
{
    data: "status",
    className: "center",
    render: function(data, type, row, meta){
         // if(type === 'display'){
            data='<td><div class="row"><div class="col-sm-5"><h6 style = "margin-top: -3px;">'+row.Status+'</h6><a href="#" data-toggle="modal" data-target="#DMTTransfersReports" onclick = DMTReports("'+row._id+'")>ViewDetails</a></div></td>';
        //}
      return data;
    }
},
],
} );

}

function DMTReports(id){
$.ajax({
 type: 'POST',
 data: { dmtid : id },
 url: '/GetDMTTranscationDetails',
 dataType   : "json",
 success: function(res){

   if(res.Status == "1"){
         walletID(res);
         $("#WalletNumber").show();
   }else if(res.Status == "0"){
      $("#WalletNumber").hide();
   }
    $("#DMTTransfersReports").modal('show');
    $('#emailID').hide();
    $('#sendbtn').hide();
    $("#RRN").html(res.ReferenceNumber);
    //$("#RemmiterMobile").html(res.RemmiterMobile);
    $("#BeneficiaryName").html(res.Name);
    $("#Bank").html(res.Bank);
    $("#AccountNumber").html(res.AccountNumber);
    $("#Ifsc").html(res.Ifsc);
    $("#PaymentType").html(res.PaymentType);

    $("#TransactionAmount").html(res.Amount);
   // $("#ResponseMessage").html(res.ResponseMessage);
    var data = moment(res.CreateDate).format("DD MMM  YYYY, h:mm A");
    $("#CreateDate").html(data);
//   }
  }
});
}
function AllRefernceNumberId(refNumber){
$.ajax({
 type: 'POST',
 data: { ReferenceNumber : refNumber },
 url: '/GetRefernceNumberReportDMT',
 dataType   : "json",
 success: function(res){
   if(res.Status == "1"){
         walletID(res);
         $("#WalletNumber").show();
   }else if(res.Status == "0"){
      $("#WalletNumber").hide();
   }
    $("#DMTTransfersReports").modal('show');
    $('#emailID').hide();
    $('#sendbtn').hide();
     $("#RRN").html(res.ReferenceNumber);
    $("#RemmiterMobile").html(res.RemmiterMobile);
    $("#BeneficiaryName").html(res.Name);
    $("#Bank").html(res.Bank);
    $("#AccountNumber").html(res.AccountNumber);
    $("#Ifsc").html(res.Ifsc);
    $("#PaymentType").html(res.PaymentType);
    $("#TransactionAmount").html(res.Amount);
    $("#ResponseMessage").html(res.ResponseMessage);
    var data = moment(res.CreateDate).format("DD MMM  YYYY, h:mm A");
    $("#CreateDate").html(data);
//   }
  }
});
}


function walletID(res){
  //alert("iii");
  $("#TransactionId").val(res.ReferenceNumber);
  $("#ServicessName").val('Money Transfer');
  $("#myModalwallet").modal('hide');
}
$("#sendEmail").click(function(){
  var data = {};
  data.ToAddress = $("#mailtoagent").val();
  data.Subject = $("#subjectmail").val();
  data.ServiceName = $("#ServicessName").val();
  data.TransactionId = $("#TransactionId").val();
  data.Email_Body = $('#bodymail').val();
  console.log(data);
  if(data.ToAddress == ""){
      $("#mailtoagent").notify("Please Enter AgentEmail..", { className: "error", position:"top" });
      return false;
  }
  if(data.Subject == ""){
      $("#subjectmail").notify("Please fill this field", { className: "error", position:"top" });
      return false;
  }
console.log(data);
//return false;
  $.ajax({
      type: 'POST',
      data: JSON.stringify(data),
      contentType: 'application/json',
      url: '/Sendmailtocustomerservices',
      async:true,
      crossDomain:true,
      success: function(res) {
        if(res == "1"){
          $("#sendEmail").notify("Message Sent Successfully..", { className: "success", position:"left" });
          setTimeout(function() {
           location.reload();
         },3000);
          // $("#subjectmail").val("");
          // $('#bodymail').val("");
          return false;
        }
        else{
          $("#sendEmail").notify("Message Not Sent..", { className: "error", position:"left" });
           $('#sendemailform')[0].reset();
          return false;
        }
      }
  });
})

function GetNiharAgent_PanCard_Reports(){
var urldata = JSON.parse('{"' + decodeURI(location.search.substring(1).replace(/%2C/g, ",").replace(/&/g, "\",\"").replace(/%23/g, "#").replace(/=/g, "\":\"") + '"}'));

data = {};
data.id = urldata.aid;

      $('#example5').dataTable({
      processing: true,
      destroy:true,
      autoWidth:false,
      jQueryUI: true,
      sAjaxDataProp: 'data',
      ajax:{
      url: "/GetNiharAgentPanCardReports",
      type: 'POST',
      data: data,
      dataSrc: ""
      },
      columns: [
        {
              data: "opr_id",
              className: "center",
              render: function(data, type, row, meta){
                 //if(type === 'display'){
                   data='<td><div class="row" style=" margin-left: -7px;"><a href="#"  data-toggle="modal" data-target="#PanCardReports" onclick = PanCardOrderReports("'+row.ipay_id+'")>'+row.ipay_id+'</a></div></td>';
                // }
                 return data;
              }
           },
      { data : "app_name" },
      // { data : "Email" },
      // { data : "PinCode" },
      { data : "app_number" },
      //{data  : "ApplicationID"},
      {
      data: "CreatedDate",
      className: "center",
      render: function(data, type, row, meta){
            //if(type === 'display'){
              data='<td>'+moment(row.CreatedDate).format("DD MMM  YYYY, h:mm A")+'</td>';
          //}
        return data;
      }
      },
      {
          data: "Status",
          className: "center",
          render: function(data, type, row, meta){
              //  if(type === 'display'){
                    data='<td><div class="row"><div class="col-sm-5"><h6 style = "margin-top: -3px;">'+row.Status+'</h6><a href="#"  data-toggle="modal" data-target="#PanCardReports" onclick = PanCardReports("'+row._id+'")>ViewDetails</a></div></td>';
            //  }
            return data;
          }
      },
      ]
      } );
    }
      function PanCardReports(id){
         $.ajax({
           type: 'POST',
            data: {panCardId:id},
            dataType:'json',
            url: '/GetPanCardViewDetailsReports',
             success: function(res) {
            if(res.Status == "1"){
                  walletID(res);
                  $("#WalletNumber").show();
            }else if(res.Status == "0"){
               $("#WalletNumber").hide();
            }
               $("#PanCardReports").modal('show');
               $('#emailID').hide();
               $('#sendbtn').hide();
               $("#name").html(res.app_name);
              // $("#email").html(res.Email);
               $("#MobileNumbersss").html(res.app_number);
               $("#applicationId").html(res.ipay_id);
               $("#PanCardStatus").html(res.resstatus);
               var data = moment(res.CreatedDate).format("DD MMM  YYYY, h:mm A");
                 $("#CreatedDate").html(data);
             }
         });
        }

        function PanCardOrderReports(appId){
           $.ajax({
             type: 'POST',
              data: {ApplicationId:appId},
              dataType:'json',
              url: '/GetPanCardOrderReportsDetails',
               success: function(res) {
                 if(res.Status == "1"){
                       walletID(res);
                       $("#WalletNumber").show();
                 }else if(res.Status == "0"){
                    $("#WalletNumber").hide();
                 }
                 $("#PanCardReports").modal('show');
                 $('#emailID').hide();
                 $('#sendbtn').hide();
                 $("#name").html(res.app_name);
                 //$("#email").html(res.Email);
                 $("#MobileNumbersss").html(res.app_number);
                 $("#applicationId").html(res.ipay_id);
                 $("#PanCardStatus").html(res.resstatus);
                 var data = moment(res.CreatedDate).format("DD MMM  YYYY, h:mm A");
                   $("#CreatedDate").html(data);
               }
           });
          }

          function walletID(res){
            //alert("iii");
            $("#TransactionId").val(res.ipay_id);
            $("#ServicessName").val('PanCard');
            $("#myModalwallet").modal('hide');
          }
          $("#sendEmail").click(function(){
            var data = {};
            data.ToAddress = $("#mailtoagent").val();
            data.Subject = $("#subjectmail").val();
            data.ServiceName = $("#ServicessName").val();
            data.TransactionId = $("#TransactionId").val();
            data.Email_Body = $('#bodymail').val();
            console.log(data);
            if(data.ToAddress == ""){
                $("#mailtoagent").notify("Please Enter AgentEmail..", { className: "error", position:"top" });
                return false;
            }
            if(data.Subject == ""){
                $("#subjectmail").notify("Please fill this field", { className: "error", position:"top" });
                return false;
            }
          console.log(data);
          //return false;
            $.ajax({
                type: 'POST',
                data: JSON.stringify(data),
                contentType: 'application/json',
                url: '/Sendmailtocustomerservices',
                async:true,
                crossDomain:true,
                success: function(res) {
                  if(res == "1"){
                    $("#sendEmail").notify("Message Sent Successfully..", { className: "success", position:"left" });
                    setTimeout(function() {
                     location.reload();
                   },3000);
                    // $("#subjectmail").val("");
                    // $('#bodymail').val("");
                    return false;
                  }
                  else{
                    $("#sendEmail").notify("Message Not Sent..", { className: "error", position:"left" });
                     $('#sendemailform')[0].reset();
                    return false;
                  }
                }
            });
          });
//}
function GetNiharAgentOnline_RTI_Reports() {
  var urldata = JSON.parse('{"' + decodeURI(location.search.substring(1).replace(/%2C/g, ",").replace(/&/g, "\",\"").replace(/%23/g, "#").replace(/=/g, "\":\"") + '"}'));

  data = {};
  data.id = urldata.aid;


$('#example6').dataTable({
      processing: true,
      destroy:true,
      autoWidth:false,
      jQueryUI: true,
      sAjaxDataProp: 'data',
      ajax:{
      url: "/GetNiharAgent_Online_RTI_Reports",
      type: 'POST',
      data: data,
      dataSrc: ""
      },
      columns: [
        {
              data: "ApplicationId",
              className: "center",
              render: function(data, type, row, meta){
              //   if(type === 'display'){
                   data='<td><div class="row" style="margin-left:15px;"><a href="#"  data-toggle="modal" data-target="#OnlineRTIReportsModel" onclick = OnlineRTIOrderReports("'+row.ApplicationId+'")>'+row.ApplicationId+'</a></div></td>';
              //   }
                 return data;
              }
           },
      { data : "FirstName",
         className:"center",
         render: function(data,type,row,meta){
           data = '<td>'+row.FirstName+' '+row.LastName+'</td>';
           return data;
         }
    },
      { data : "Email" },
      { data : "Mobile" },
      { data : "PinCode" },
    //{ data : "RTIStatus" },
    //  {data  :"ApplicationId"},
      {
         data: "createdAt",
         className: "center",
         render: function(data, type, row, meta){
              // if(type === 'display'){
                 data='<td>'+moment(row.createdAt).format("DD MMM  YYYY, h:mm A")+'</td>';
            // }
           return data;
         }
        },
        {
              data: "Payment",
             className: "center",
             render: function(data, type, row, meta){

                    if(type === 'display'){
                      if (row.Payment=='No') {
                        data = '<a onclick = paymentmodal("'+encodeURIComponent(row._id)+'","'+encodeURIComponent(row.AgentId)+'","'+encodeURIComponent(row.ApplicationId)+'") >Payment</a>';
                      }
                      if (row.Payment=='success') {
                        data = '<td><h4 style = "margin-top: -3px;">'+row.Payment+'</h4><a href="#"  data-toggle="modal" data-target="#OnlineRTIReportsModel" onclick = OnlineRTIReports("'+row._id+'")>ViewDetails</a></td>';
                      }
                    }
                    return data;
                 }
        },
      ]
} );
}
function OnlineRTIReports(id){
  alert(id)
   $.ajax({
      type: 'POST',
      data: {onlineRTIId:id},
      url: '/getOnlineRTIID',
      dataType:'json',
       success: function(res) {
               if(res.Status == "1"){
                     walletID(res);
                     $("#WalletNumber").show();
               }else if(res.Status == "0"){
                  $("#WalletNumber").hide();
               }
         $("#OnlineRTIReportsModel").modal('show');
         $('#emailID').hide();
         $('#sendbtn').hide();
         $("#name").html(res.UserName);
         $("#email").html(res.Email);
         $("#MobileNumber").html(res.Mobile);
         $("#applicationId").html(res.ApplicationId);
         $("#paymentStatus").html(res.Payment);
         var data = moment(res.createdAt).format("DD MMM  YYYY, h:mm A");
           $("#CreatedDate").html(data);

       }
   });
  }

  function OnlineRTIOrderReports(orderID){
    alert(orderID)
     $.ajax({
        type: 'POST',
        data: {AppID:orderID},
        url: '/GetOnlineOrderReportsDetails',
        dataType:'json',
         success: function(res) {
                 if(res.Status == "1"){
                       walletID(res);
                       $("#WalletNumber").show();
                 }else if(res.Status == "0"){
                    $("#WalletNumber").hide();
                 }
           $("#OnlineRTIReportsModel").modal('show');
           $('#emailID').hide();
           $('#sendbtn').hide();
           $("#name").html(res.UserName);
           $("#email").html(res.Email);
           $("#MobileNumber").html(res.Mobile);
           $("#applicationId").html(res.ApplicationId);
           $("#paymentStatus").html(res.Payment);
           var data = moment(res.createdAt).format("DD MMM  YYYY, h:mm A");
             $("#CreatedDate").html(data);

         }
     });
    }

function paymentmodal(id,AgentId,applicationId){
  $("#OnlineRTIId").val(id);
  $("#AgentId").val(AgentId);
  $("#appnumbertxt").val(applicationId)
  $("#diretpaymentmodal").modal('show');
return false;
}
$("#paymentbtn").click(function(){
  var data = {};
  data.paymenttype = $('input[name=group5]:checked').val();
  data.appnumber = $("#appnumberlbl").html();

  $.ajax({
      type: 'POST',
      data: JSON.stringify(data),
      contentType: 'application/json',
      url: '/RTIPayment',
      async:false,
      crossDomain:true,
      success: function(res) {
        if(res == "1"){
          $("#paymentbtn").notify("success... You will get confirmation call", { className: "success", position:"right" });
          setTimeout(function(){
            $("#paymentmodal").modal("hide");
           }, 5000);
          return false;
        }else if(res == "3"){
          $("#paymentbtn").notify("Insufficient Balance", { className: "error", position:"right" });
          return false;
        }
        else{
          $("#paymentbtn").notify("Some thing went wrong..Please try after some time", { className: "error", position:"right" });
          return false;
        }
      }
  });
  return false;
})

function walletID(res){
  //alert("iii");
  $("#TransactionId").val(res.ApplicationId);
  $("#ServicessName").val('Online RTI');
  $("#myModalwallet").modal('hide');
}
$("#sendEmail").click(function(){
  var data = {};
  data.ToAddress = $("#mailtoagent").val();
  data.Subject = $("#subjectmail").val();
  data.ServiceName = $("#ServicessName").val();
  data.TransactionId = $("#TransactionId").val();
  data.Email_Body = $('#bodymail').val();
  console.log(data);
  if(data.ToAddress == ""){
      $("#mailtoagent").notify("Please Enter AgentEmail..", { className: "error", position:"top" });
      return false;
  }
  if(data.Subject == ""){
      $("#subjectmail").notify("Please fill this field", { className: "error", position:"top" });
      return false;
  }
console.log(data);
//return false;
  $.ajax({
      type: 'POST',
      data: JSON.stringify(data),
      contentType: 'application/json',
      url: '/Sendmailtocustomerservices',
      async:true,
      crossDomain:true,
      success: function(res) {
        if(res == "1"){
          $("#sendEmail").notify("Message Sent Successfully..", { className: "success", position:"left" });
          setTimeout(function() {
           location.reload();
         },3000);
          // $("#subjectmail").val("");
          // $('#bodymail').val("");
          return false;
        }
        else{
          $("#sendEmail").notify("Message Not Sent..", { className: "error", position:"left" });
           $('#sendemailform')[0].reset();
          return false;
        }
      }
  });
})

function GetNiharAgent_Hotel_Booking_Reports(){
  var urldata = JSON.parse('{"' + decodeURI(location.search.substring(1).replace(/%2C/g, ",").replace(/&/g, "\",\"").replace(/%23/g, "#").replace(/=/g, "\":\"") + '"}'));

  data = {};
  data.id = urldata.aid;
$('#example8').DataTable({
processing: true,
destroy:true,
autoWidth:false,
jQueryUI: true,
sAjaxDataProp: 'data',
ajax:{
url: "/GetNiharAgent_Hotel_Booking_Reports",
type: 'POST',
data: data,
dataSrc: ""
},
columns: [
{ data : "external_reference_id",
 render:function(type,data,row,meta){
   data='<td><div class="row" style=" margin-left: -7px;"><a href="#" data-toggle="modal" data-target="#HotelBookingsReportsModal" onclick = HotelBookingsOrderID("'+row.external_reference_id+'")>'+row.external_reference_id+'</a></div></td>';
return data;
 }


 },
{ data : "guest_name" },
{ data : "guest_phone" },
{ data : "roomCount" },
{ data : "checkin"},
{ data : "checkout"},
{ data : "TotalAmount"},
//{ data : "Created_Date"},
{
   data: "Created_Date",
   //className: "center",
   render: function(data, type, row, meta){
      //   if(type === 'display'){
           data='<td>'+moment(row.Created_Date).format("D MMM YYYY HH:mm A")+'</td>';
      // }
     return data;
   }
   },

{ data:'status',

 render:function(type,data,row,meta){

   data='<td><div class="row"><div class="col-sm-5"><h6 style = "margin-top: -3px;">'+row.status+'</h6><a href="#" data-toggle="modal" data-target="#HotelBookingsReportsModal" onclick = HotelBookingsReportsDetails("'+row._id+'")>ViewDetails</a></div></td>';

return data;
 }
},
{
data: "CancellationStatus",
className: "center",
render: function(data, type, row, meta){
    //  if(type === 'display'){
        if(row.CancellationStatus == ""){
          data = "<a href='#' onclick=cancelrequest("+row.id+",'"+row.external_reference_id+"','"+row.guest_name+"','"+row.roomCount+"','"+row.checkin+"','"+row.checkout+"','"+row.TotalAmount+"')>Cancel</a>";
        }else{
          data = "<a href>Cancelled</a>";
        }
  //  }
  return data;
}
},
],
} );
}

function cancelrequest(id,external_reference_id,guest_name,roomCount,checkin,checkout,TotalAmount){
  $("#CancelReqModal").modal("show");
  $("#hid").val(id);
  $("#referenceid").html(external_reference_id);
  $("#guestname").html(guest_name);
  $("#Noofrooms").html(roomCount);
  $("#ckeckin").html(roomCount);
  $("#checkout").html(roomCount);
  $("#TotalAmount").html(TotalAmount);
  var data = {};
  data.id = id;
  data.referenceid = external_reference_id;
  $.ajax({
      type: 'POST',
      data: JSON.stringify(data),
      contentType: 'application/json',
      url: '/getcancellationcharges',
      async:false,
      crossDomain:true,
      success: function(res) {
        $("#AmountRefund").html(TotalAmount - res);
      }
  });
  return false;
}

$("#Cancelhotel_BTN").click(function(){
  var data = {};
  data.id = $("#hid").val();
  data.referenceid = $("#referenceid").html();
  data.totalcancellation = $("#AmountRefund").html();
  $.ajax({
      type: 'POST',
      data: JSON.stringify(data),
      contentType: 'application/json',
      url: '/ConfirmCancel',
      async:false,
      crossDomain:true,
      success: function(res) {
        console.log(res);
        if(res == "1"){
          $("#Cancelhotel_BTN").notify("Booking Cancelled..", { className: "success", position:"right" });
          setTimeout(function(){ location.reload() }, 3000);
          return false;
        }else{
          $("#Cancelhotel_BTN").notify(res, { className: "error", position:"right" });
          return false;
        }
      }
  });
})
function HotelBookingsReportsDetails(id){
   $.ajax({
     type: 'POST',
      data: {panCardId:id},
      dataType:'json',
      url: '/getHotelBookingsViewDetailsReports',
       success: function(res) {
         console.log(res);
      if(res.status == "1"){
            walletID(res);
            $("#WalletNumber").show();
      }else if(res.status == "0"){
         $("#WalletNumber").hide();
      }
         $("#HotelBookingsReportsModal").modal('show');
         $("#refNumberID").html(res.external_reference_id);
         $("#GuestName").html(res.guest_name);
         $("#guestMobileNumber").html(res.guest_phone);
         $("#numberGuestRooms").html(res.roomCount);
         $("#checkinDate").html(res.checkin);
         $("#CheckoutDate").html(res.checkout);
         $("#TotalAmountHotels").html(res.TotalAmount);
         $("#HotelBookingsStatus").html('Hotel Booked Successfully');
         var data = moment(res.CreatedDate).format("DD MMM  YYYY, h:mm A");
           $("#BookingDate").html(data);
       }
   });
  }

  function HotelBookingsOrderID(appId){
     $.ajax({
       type: 'POST',
        data: {ApplicationId:appId},
        dataType:'json',
        url: '/GetHotelBookingOrderReportsDetails',
         success: function(res) {
           if(res.status == "1"){
                 walletID(res);
                 $("#WalletNumber").show();
           }else if(res.status == "0"){
              $("#WalletNumber").hide();
           }
           $("#HotelBookingsReportsModal").modal('show');
           $("#refNumberID").html(res.external_reference_id);
           $("#GuestName").html(res.guest_name);
           $("#guestMobileNumber").html(res.guest_phone);
           $("#numberGuestRooms").html(res.roomCount);
           $("#checkinDate").html(res.checkin);
           $("#CheckoutDate").html(res.checkout);
           $("#TotalAmountHotels").html(res.TotalAmount);
           $("#HotelBookingsStatus").html('Hotel Booked Successfully');
           var data = moment(res.CreatedDate).format("DD MMM  YYYY, h:mm A");
             $("#BookingDate").html(data);
         }
     });
    }
    function walletID(res){
      //alert("iii");
      $("#TransactionId").val(res.external_reference_id);
      $("#ServicessName").val('HotelBooked Successfully');
      $("#myModalwallet").modal('hide');
    }
    $("#sendEmail").click(function(){
      var data = {};
      data.ToAddress = $("#mailtoagent").val();
      data.Subject = $("#subjectmail").val();
      data.ServiceName = $("#ServicessName").val();
      data.TransactionId = $("#TransactionId").val();
      data.Email_Body = $('#bodymail').val();
      console.log(data);
      if(data.ToAddress == ""){
          $("#mailtoagent").notify("Please Enter AgentEmail..", { className: "error", position:"top" });
          return false;
      }
      if(data.Subject == ""){
          $("#subjectmail").notify("Please fill this field", { className: "error", position:"top" });
          return false;
      }
    console.log(data);
    //return false;
      $.ajax({
          type: 'POST',
          data: JSON.stringify(data),
          contentType: 'application/json',
          url: '/Sendmailtocustomerservices',
          async:true,
          crossDomain:true,
          success: function(res) {
            if(res == "1"){
              $("#sendEmail").notify("Message Sent Successfully..", { className: "success", position:"left" });
              setTimeout(function() {
               location.reload();
             },3000);
              // $("#subjectmail").val("");
              // $('#bodymail').val("");
              return false;
            }
            else{
              $("#sendEmail").notify("Message Not Sent..", { className: "error", position:"left" });
               $('#sendemailform')[0].reset();
              return false;
            }
          }
      });
    })

    function GetNiharAdminFlightBookingDetails(){
      var urldata = JSON.parse('{"' + decodeURI(location.search.substring(1).replace(/%2C/g, ",").replace(/&/g, "\",\"").replace(/%23/g, "#").replace(/=/g, "\":\"") + '"}'));

      data = {};
      data.id = urldata.aid;
      $.fn.dataTable.ext.errMode = 'none';
      $('#example9').DataTable({
      processing: true,
      destroy:true,
      autoWidth:false,
      jQueryUI: true,
      sAjaxDataProp: 'data',
      ajax:{
      url: "/GetNiharAgent_Flight_Booking_Reports",
      type: 'POST',
      data: data,
      dataSrc: ""
      },
      columns: [
      { data : "pnr",
       render:function(type,data,row,meta){
       //  console.log(row.pnr[0]);
         data='<td><div class="row" style=" margin-left: -7px;"><a href="#" onclick = FlightBookingsOrderID("'+row.pnr+'")>'+row.pnr+'</a></div></td>';
      return data;
       }
       },
       { data : "name",
         className:"center",
         render:function(type,data,row,meta){
         var txt = '';
           $.each(row.PassengerList,function(i,v) {
                 if (txt.length > 0) {
                   txt += ' '
                 }
                 txt += '<td><div class="row" style="margin-left: 10px;">'+v.Surname+' '+v.name+'</div></td><br>';
             });
             return txt;
         }
      },
      { data : 'phone'},
      { data : "name",
        className :"center",
        render:function(type,data,row,meta){
          data = '<td><div class="row"  style="margin-left: -10px;">'+row.flightDetails[0].name+'</div></td><br>';
          return data;
        }

    },

     { data : 'Fare'},
     { data : 'Airportname',
          className:'center',
          render:function(type,data,row,type){
            data ='<td><div class="row"  style="margin-left: -9px;">'+row.flightDetails[0].Departure.Airportname+'</div></td>';
            return data;
          }
     },
     { data : 'AirportName',
          className:'center',
          render:function(type,data,row,type){
            data ='<td><div class="row" style ="margin-left: -3px;" >'+row.flightDetails[0].Arriaval.AirportName+'</div></td>';
            return data;
          }
     },

     { data : 'date',
          className:'center',
          render:function(type,data,row,type){
            data ='<td><div class="row"  style="margin-left: -9px;">'+(row.flightDetails[0].Departure.date).split("T")+'</div></td>';
            return data;
          }
     },
     { data : 'date',
          className:'center',
          render:function(type,data,row,type){
            data ='<td><div class="row"  style="margin-left: -9px;">'+(row.flightDetails[0].Arriaval.date).split("T")+'</div></td>';
            return data;
          }
     },
      { data:'status',
       render:function(type,data,row,meta){
         data='<td><div class="row"><div class="col-sm-5"><h6 style = "margin-top: -3px;">'+row.status+'</h6><a href="#" onclick = HotelBookingsReportsDetails("'+row._id+'")>ViewDetails</a></div></td>';

         return data;
       }
      },
      {
      data: "CancellationStatus",
      className: "center",
      render: function(data, type, row, meta){
              if(row.CancellationStatus == ""){
                data = "<a href='#' onclick=cancelrequest("+encodeURIComponent(row.id)+",'"+encodeURIComponent(row.external_reference_id)+"','"+encodeURIComponent(row.guest_name)+"','"+encodeURIComponent(row.roomCount)+"','"+encodeURIComponent(row.checkin)+"','"+encodeURIComponent(row.checkout)+"','"+encodeURIComponent(row.TotalAmount)+"')>Cancel</a>";
              }else{
                data = "<a href>Cancelled</a>";
              }
        return data;
      }
      },
      ],
      } );
    }

    function HotelBookingsReportsDetails(id){
       $.ajax({
         type: 'POST',
          data: {flightId:id},
          dataType:'json',
          url: '/getFlightBookingsViewDetailsReports',
           success: function(res) {
             console.log(res);
          if(res.status == "1"){
                walletID(res);
                $("#WalletNumber").show();
          }else if(res.status == "0"){
             $("#WalletNumber").hide();
          }
             $("#flightBookingsReportsModal").modal('show');
             $("#pnrNumber").html(res.pnr);
             $("#passingerName").html(res.PassengerList[0].Surname+' '+res.PassengerList[0].name);
             $("#passingerMobile").html(res.phone);
             $("#flightName").html(res.flightDetails[0].name);
             $("#fareAmount").html(res.Fare);
             $("#source").html(res.flightDetails[0].Departure.Airportname);
             $("#destination").html(res.flightDetails[0].Arriaval.AirportName);
             $("#DepartureDate").html('<td><div class="row">'+(res.flightDetails[0].Departure.date).split("T")+'</div></td>');
             $("#ArriavalDate").html('<td><div class="row">'+(res.flightDetails[0].Arriaval.date).split("T")+'</div></td>');

             $("#flightBookingsStatus").html('Flight Booked Successfully');
             // var data = moment(res.CreatedDate).format("DD MMM  YYYY, h:mm A");
             //   $("#BookingDate").html(data);
           }
       });
      }

    function FlightBookingsOrderID(transId){
       $.ajax({
         type: 'POST',
          data: {ApplicationId:transId},
          dataType:'json',
          url: '/GetFlightBookingOrderReportsDetails',
           success: function(res) {
             console.log(res);
            // var date = (res.flightDetails[0].Departure.date).split("T");

             if(res.status == "1"){
                   walletID(res);
                   $("#WalletNumber").show();
             }else if(res.status == "0"){
                $("#WalletNumber").hide();
             }
             $("#flightBookingsReportsModal").modal('show');
             $("#pnrNumber").html(res.pnr);
             $("#passingerName").html(res.PassengerList[0].Surname+' '+res.PassengerList[0].name);
             $("#passingerMobile").html(res.phone);
             $("#flightName").html(res.flightDetails[0].name);
             $("#fareAmount").html(res.Fare);
             $("#source").html(res.flightDetails[0].Departure.Airportname);
             $("#destination").html(res.flightDetails[0].Arriaval.AirportName);
             $("#DepartureDate").html('<td><div class="row">'+(res.flightDetails[0].Departure.date).split("T")+'</div></td>');
             $("#ArriavalDate").html('<td><div class="row">'+(res.flightDetails[0].Arriaval.date).split("T")+'</div></td>');

             //$("#ArriavalDate").html((res.flightDetails[0].Arriaval.Date).split("T"));
             $("#flightBookingsStatus").html('Flight Booked Successfully');

           }
       });
      }
      function walletID(res){
        //alert("iii");
        $("#TransactionId").val(res.pnr);
        $("#ServicessName").val('FlightBookedDetails Successfully');
        $("#myModalwallet").modal('hide');
      }
      $("#sendEmail").click(function(){
        var data = {};
        data.ToAddress = $("#mailtoagent").val();
        data.Subject = $("#subjectmail").val();
        data.ServiceName = $("#ServicessName").val();
        data.TransactionId = $("#TransactionId").val();
        data.Email_Body = $('#bodymail').val();
        console.log(data);
        if(data.ToAddress == ""){
            $("#mailtoagent").notify("Please Enter AgentEmail..", { className: "error", position:"top" });
            return false;
        }
        if(data.Subject == ""){
            $("#subjectmail").notify("Please fill this field", { className: "error", position:"top" });
            return false;
        }
      console.log(data);
      //return false;
        $.ajax({
            type: 'POST',
            data: JSON.stringify(data),
            contentType: 'application/json',
            url: '/Sendmailtocustomerservices',
            async:true,
            crossDomain:true,
            success: function(res) {
              if(res == "1"){
                $("#sendEmail").notify("Message Sent Successfully..", { className: "success", position:"left" });
                setTimeout(function() {
                 location.reload();
               },3000);
                // $("#subjectmail").val("");
                // $('#bodymail').val("");
                return false;
              }
              else{
                $("#sendEmail").notify("Message Not Sent..", { className: "error", position:"left" });
                 $('#sendemailform')[0].reset();
                return false;
              }
            }
        });
      })
