Getitdukaan_ts();
var res_orders;
function Getitdukaan_ts() {
  console.log('fn');
  $.ajax({
            dataType : 'json',
            type : 'POST',
            url : '/Getitdukaan_ts',
            success : function (res) {
              console.log(res);
              res_orders=res;
              var txt='';
              for (var i = 0; i < res.length; i++) {
                var inv_l=res[i].Products[0].invoice.length;
                console.log(inv_l);
                 txt+='<tr><td>'+res[i].Products[0].order_id+'</td><td>'+res[i].AgentName+'</td><td>'+moment(res[i].created_at).format("DD/MM/YYYY")+'</td><td>';
                 for (var s = 0; s < inv_l; s++) {
                   var del_latest_open=res[i].Products[0].invoice[s].delivery_status.length-1;
                   var color_btn='';
                   if (res[i].Products[0].invoice[s].delivery_status[del_latest_open].Review=='No'&&res[i].Products[0].invoice[s].delivery_status[del_latest_open].Dispatch=='No') {
                     color_btn='background:red;color:white;';
                   }else if (res[i].Products[0].invoice[s].delivery_status[del_latest_open].Review=='Yes'&&res[i].Products[0].invoice[s].delivery_status[del_latest_open].Dispatch=='No') {
                     color_btn='background:orange;color:white;';
                   }else if (res[i].Products[0].invoice[s].delivery_status[del_latest_open].Review=='Yes'&&res[i].Products[0].invoice[s].delivery_status[del_latest_open].Dispatch=='Yes') {
                     color_btn='background:green;color:white;';
                   }
                   txt+='<button type="button" class="btn" style="margin:10px;'+color_btn+'" onclick="Generate_invoice('+i+','+s+')">Get Invoice</button>';
                 }
                 txt+='</td></tr>';
              }
              $('#add_invoicedata').html(txt);

            },error:function (err) {
              alert('Server error..Please try again');
                console.log(err);
            }
  });
}
  function change_stage(a,b) {
    console.log(a,b);
    var id_order=$('#id_itdukaan_order').val();
    var data={};
    data.stage=a;data.status=b;
    data.id=id_order;
      $.ajax({
        data:data,
              dataType : 'json',
              type : 'POST',
              url : '/change_stage',
              success : function (res) {
                console.log(res);
                if (res=='closed') {
                  alert('Invoice already closed');
                  $('#myModal_invoice').modal('hide');
                }else if (res.ok==1) {
                  alert('Status changed');
                    $('#myModal_invoice').modal('hide');
                }else {
                    alert('Please try again');
                    $('#myModal_invoice').modal('hide');
                }
              },
              error:function(err) {
                console.log(err);
              }
    });
  }
  function Generate_invoice(a,b) {
    $('#id_itdukaan_order').val(res_orders[a]._id);
    $('#orderid').text(res_orders[a].Products[0].order_id);
    $('.orderdate').text(moment(res_orders[a].created_at).format("DD/MM/YYYY"));
    $('#seller_cardinfo').html('<strong class="text-red">PAN/GST</strong><br>PAN: '+res_orders[a].Products[0].invoice[b].seller.PAN+'<br>GST: '+res_orders[a].Products[0].invoice[b].seller.GST+'<br>');
    $('#seller_addinfo').html('<strong class="text-red">Sold By</strong><br>'+res_orders[a].Products[0].invoice[b].seller.Name+'<br>'+res_orders[a].Products[0].invoice[b].seller.billing_address.Address+'<br>City: '+res_orders[a].Products[0].invoice[b].seller.billing_address.City+', State: '+res_orders[a].Products[0].invoice[b].seller.billing_address.State+', Pin: '+res_orders[a].Products[0].invoice[b].seller.billing_address.Pincode+'.');

    $('#buyer_cardinfo').html('<strong class="text-red">PAN/GST</strong><br>PAN: '+res_orders[a].Products[0].invoice[b].buyer.PAN+'<br>GST: '+res_orders[a].Products[0].invoice[b].buyer.GST+'<br>');
    $('#buyer_addinfo_b').html('<strong class="text-red">Billing Address</strong><br>'+res_orders[a].Products[0].invoice[b].buyer.billing_address['Billing Name']+'<br>'+res_orders[a].Products[0].invoice[b].buyer.billing_address.Address+'<br>City: '+res_orders[a].Products[0].invoice[b].buyer.billing_address.City+', State: '+res_orders[a].Products[0].invoice[b].buyer.billing_address.State+', Pin: '+res_orders[a].Products[0].invoice[b].buyer.billing_address.Pincode+'.');
    $('#buyer_addinfo_s').html('<strong class="text-red">Shipping Address</strong><br>'+res_orders[a].Products[0].invoice[b].buyer.shipping_address['Shipping Name']+'<br>'+res_orders[a].Products[0].invoice[b].buyer.billing_address.Address+'<br>City: '+res_orders[a].Products[0].invoice[b].buyer.shipping_address.City+', State: '+res_orders[a].Products[0].invoice[b].buyer.shipping_address.State+', Pin: '+res_orders[a].Products[0].invoice[b].buyer.shipping_address.Pincode+'.');
    $('#invoice_id').text(res_orders[a].Products[0].invoice[b].invoice_id);
    var items=res_orders[a].Products[0].invoice[b].orderItems;
    var msg='';
    var all_items_tax=0;
    var all_items_price=0;
    var shipping_amt=0;
    for (var i = 0; i < items.length; i++) {
      msg+='<tr><td>'+(i+1)+'</td><td>'+items[i].product_id+'</td><td>'+items[i].product_name+'</td><td class="text-right"><i class="fa fa-inr"></i>'+items[i].price+'</td><td class="text-right">'+items[i].qty+'</td><td class="text-right">'+items[i].cgst_amt+' ('+items[i].cgst_pct+' %)</td><td class="text-right">'+items[i].sgst_amt+' ('+items[i].sgst_pct+' %)</td><td class="text-right">'+items[i].igst_amt+' ('+items[i].igst_pct+' %)</td><td class="text-right">'+items[i].utgst_amt+' ('+items[i].utgst_pct+' %)</td>';

      var totaltax_per=items[i].cgst_pct+items[i].sgst_pct+items[i].igst_pct+items[i].utgst_pct;
      var totaltax_amt=items[i].cgst_amt+items[i].sgst_amt+items[i].igst_amt+items[i].utgst_amt;

      all_items_tax+=totaltax_amt;
       shipping_amt+=items[i].shipping_cgst_amt+items[i].shipping_igst_amt+items[i].shipping_sgst_amt+items[i].shipping_utgst_amt;
      var totalcost=totaltax_amt+parseFloat(items[i].price);
      all_items_price += parseFloat(items[i].price);

      msg+='<td class="text-right"><i class="fa fa-inr"></i>'+totaltax_amt+' ('+totaltax_per+' %)</td><td class="text-right"><i class="fa fa-inr"></i>'+totalcost+'</td></tr>'
    }
      var cls_btn='';
      var btn_name='';
      var del_status=0;
      var del_latest=res_orders[a].Products[0].invoice[b].delivery_status.length-1;
      console.log(del_latest);
      if (res_orders[a].Products[0].invoice[b].delivery_status[del_latest].Review=='No'&&res_orders[a].Products[0].invoice[b].delivery_status[del_latest].Dispatch=='No') {
        cls_btn='btn-danger';
        btn_name='Mark as Reviewed';
        del_status=1;
      }else if (res_orders[a].Products[0].invoice[b].delivery_status[del_latest].Review=='Yes'&&res_orders[a].Products[0].invoice[b].delivery_status[del_latest].Dispatch=='No') {
        cls_btn='btn-warning';
        btn_name='Mark as Dispatched';
        del_status=2;
      }else if (res_orders[a].Products[0].invoice[b].delivery_status[del_latest].Review=='Yes'&&res_orders[a].Products[0].invoice[b].delivery_status[del_latest].Dispatch=='Yes') {
        cls_btn='btn-success';
        btn_name='Invoice closed';
        del_status=3;
      }
    $('.to_review').attr('onclick','change_stage('+b+','+del_status+')');
    $('.to_review').addClass(cls_btn);
    $('#to_review_txt').text(btn_name);
    console.log(btn_name);
    $('#Tgst').text(all_items_tax);
    $('#Shipping_amt').text(shipping_amt);
    $('#all_items_tax').text(all_items_tax);
    $('#all_items_price').text(all_items_price);
    $('#Tnetamt').text(all_items_price);
    $('#Gtotal').text((all_items_price+all_items_tax+shipping_amt).toFixed(2));
    $('#orderitems_invoice').html(msg);
    $('#myModal_invoice').modal('show');

  }
