var res_orders;
function Getitdukaan_ts12(store) {
var data = {};
data.store = store;
$('#example1').dataTable({
  processing: true,
  destroy:true,
  autoWidth:false,
  jQueryUI: true,
  sAjaxDataProp: 'data',
    ajax:{
    url: "/Getitdukaan_ts",
    type: 'POST',
    data: JSON.stringify(data),
    dataSrc: ""
    },
    columns: [
      {
      data: "_id",
      className: "center",
      render: function(data, type, row, meta){
        data=meta.row;
        return data;
        }
      },
      {
      data: "_id",
      className: "center",
      render: function(data, type, row, meta){
            if(type === 'display'){
              data='<td>'+row.Products[0].order_id+'</td>'
          }
        return data;
      }
      },
      {
      data: "_id",
      className: "center",
      render: function(data, type, row, meta){
            if(type === 'display'){
              data='<td>'+row.Products[0].order_id+'</td>'
          }
        return data;
      }
      },
    ],
});
}
function Getitdukaan_ts(store) {
  var data = {};
  data.store = store;
  $('#example1').dataTable({
  processing: true,
  destroy:true,
  autoWidth:false,
  jQueryUI: true,
  sAjaxDataProp: 'data',
  ajax:{
  url: "/Getitdukaan_ts",
  type: 'POST',
  data: data,
  dataSrc: function (res) {
    console.log(res[0].Products);
    res_orders=res;
    return res;
  },
  },
    columns: [
      { data : "_id",
       className: "center",
        render: function(data, type, row, meta){
          data=meta.row+1;
          return data;
        }
      },
      { data : "Products",
       className: "center",
        render: function(data, type, row, meta){
          data=data[0].order_id;
          return data;
        }
      },
      { data : "AgentName" },
      { data : "created_at",
        className: "center",
        render: function(data, type, row, meta){
          data=moment(data).format("DD/MM/YYYY HH:MM:SS");
          return data;
        }
      },
      { data : "Products",
         className: "center",
        render: function(data, type, row, meta){
          var txt='';
          for (var i = 0; i < data[0].invoice.length; i++) {
            var del_latest_open=data[0].invoice[i].delivery_status.length-1;
            var color_btn='';
            var statusofinvoice='';
            if (data[0].invoice[i].delivery_status[del_latest_open].Review=='No'&&data[0].invoice[i].delivery_status[del_latest_open].Dispatch=='No') {
              color_btn='background:red;color:white;';
              statusofinvoice="Processed";
            }else if (data[0].invoice[i].delivery_status[del_latest_open].Review=='Yes'&&data[0].invoice[i].delivery_status[del_latest_open].Dispatch=='No') {
              color_btn='background:orange;color:white;';
              statusofinvoice="Reviewed";
            }else if (data[0].invoice[i].delivery_status[del_latest_open].Review=='Yes'&&data[0].invoice[i].delivery_status[del_latest_open].Dispatch=='Yes') {
              color_btn='background:green;color:white;';
              statusofinvoice="Dispatched";
            }else{
              color_btn='background:red;color:white;';
              statusofinvoice="Returned";
            }
            txt+='<button type="button" class="btn" style="margin:10px;'+color_btn+'" onclick="Generate_invoice('+meta.row+','+i+')">'+statusofinvoice+'</button>';
          }
          data=txt;
          return data;
        }
      },
  ],
  });
}
function change_stage(a,b) {
    if (b == "3") {
      $("#stage").val(a);
      $("#status").val(b);
      $("#returns_modal").modal("show");
      return false;
    }
    var id_order=$('#id_itdukaan_order').val();
    var storename=$('#storename').val();
    var data={};
    data.stage=a;
    data.status=b;
    data.id=id_order;
    data.Store = storename;
    console.log(data);
      $.ajax({
        data:data,
        dataType : 'json',
        type : 'POST',
        url : '/change_stage',
        success : function (res) {
          console.log(res);
          if (res=='closed') {

          }else if (res.ok==1) {
            alert('Status changed');
            location.reload();
          }else {
            alert('Please try again');
            location.reload();
          }
        },
        error:function(err) {
        }
    });
  }

  $("#return_btn").click(function(){
    var id_order=$('#id_itdukaan_order').val();
    var data={};
    data.stage=$("#stage").val();
    data.status=$("#status").val();
    data.id=id_order;
    data.Store = "itdukaan";
    data.return_reason = $("#return_reason").val();
    data.return_description = $("#return_description").val();
    console.log(data);
      $.ajax({
        data:data,
        dataType : 'json',
        type : 'POST',
        url : '/change_stage',
        success : function (res) {
          if (res=='closed') {

          }else if (res.ok==1) {
            alert('Status changed');
            location.reload();
          }else {
            alert('Please try again');
            location.reload();
          }
        }
    })
    return false;
  })
function Generate_invoice(a,b) {
  var state = res_orders[a].Products[0].invoice[b].state;
  var other_state = res_orders[a].Products[0].invoice[b].other_state;
  var union_state = res_orders[a].Products[0].invoice[b].union_state;
    var htmlpage = '<section class="invoice printableArea">';
    htmlpage += '<div class="modal-header">'
    htmlpage += '<img src="Public3/images/logo.png" alt="">'
    htmlpage += '<div class="pull-right">'
    htmlpage += '<div class="tax-top"><b style="font-weight: 600;">'
    htmlpage += 'Tax Invoice/Bill of Supply/Cash Memo'
    htmlpage += '</b></div>'
    htmlpage += '<div class="tax-topSmal">(Triplicate)</div>'
    htmlpage += '</div></div>'
    htmlpage += '<div class="modal-body">'
    htmlpage += '<div class="row">'
    htmlpage += '<div class="col-12">'
    htmlpage += '<input type="hidden" id="id_itdukaan_order" value='+res_orders[a]._id+'>'
    htmlpage += '<h4 class="page-header" style="font-size:18px;">'
    htmlpage += 'Order Number: <span id="orderid">'+res_orders[a].Products[0].order_id+'</span>'
    htmlpage += '<small class="pull-right">Order Date: <span class="orderdate">'+moment(res_orders[a].created_at).format("DD/MM/YYYY")+'</span> </small>'
    htmlpage += '</h4></div></div>';
    htmlpage += '<div class="container" style=" margin-top: -20px;">'
    htmlpage += '<div class="row invoice-info">'
    htmlpage += '<div class="col-sm-4 invoice-col">'
    htmlpage += '<h4>Seller Details</h4>'
    htmlpage += '<address id="seller_addinfo"><strong class="text-red">Sold By</strong><br>'+res_orders[a].Products[0].invoice[b].seller.Name+'<br>'+res_orders[a].Products[0].invoice[b].seller.billing_address.Address+'<br>City: '+res_orders[a].Products[0].invoice[b].seller.billing_address.City+', State: '+res_orders[a].Products[0].invoice[b].seller.billing_address.State+', Pin: '+res_orders[a].Products[0].invoice[b].seller.billing_address.Pincode+'.'
    htmlpage += '</address>'
    htmlpage += '<address id="seller_cardinfo"><strong class="text-red">PAN/GST</strong><br>PAN: '+res_orders[a].Products[0].invoice[b].seller.PAN+'<br>GST: '+res_orders[a].Products[0].invoice[b].seller.GST+'<br>'
    htmlpage += '</address>'
    htmlpage += '</div>'
    htmlpage += '<div class="col-sm-4 invoice-col">'
    htmlpage += '<h4>Buyer Details</h4>'
    htmlpage += '<address id="buyer_addinfo_b">';
    htmlpage += '<strong class="text-red">Billing Address</strong><br>'+res_orders[a].Products[0].invoice[b].buyer.billing_address['Billing Name']+'<br>'+res_orders[a].Products[0].invoice[b].buyer.billing_address.Address+'<br>City: '+res_orders[a].Products[0].invoice[b].buyer.billing_address.City+', State: '+res_orders[a].Products[0].invoice[b].buyer.billing_address.State+', Pin: '+res_orders[a].Products[0].invoice[b].buyer.billing_address.Pincode+'.';
    htmlpage += '</address>'
    htmlpage += '<address id="buyer_cardinfo">'
    if (res_orders[a].Products[0].invoice[b].buyer.PAN == null) {
      var pansts = "Not Applicable";
    }else{
      var pansts = res_orders[a].Products[0].invoice[b].buyer.PAN;
    }
    if (res_orders[a].Products[0].invoice[b].buyer.GST == null) {
      var gststass = "Not Applicable"
    }else{
      var gststass = res_orders[a].Products[0].invoice[b].buyer.GST;
    }
    htmlpage += '<strong class="text-red">PAN/GST</strong><br>PAN: '+pansts+'<br>GST: '+gststass+'<br>'
    htmlpage += '</address>'

      htmlpage +='</div>'
      htmlpage += '<div class="col-sm-4 invoice-col" style="padding-top: 38px;">'
      htmlpage +='<address id="buyer_addinfo_s">'
      htmlpage += '<strong class="text-red">Shipping Address</strong><br>'+res_orders[a].Products[0].invoice[b].buyer.shipping_address['Shipping Name']+'<br>'+res_orders[a].Products[0].invoice[b].buyer.billing_address.Address+'<br>City: '+res_orders[a].Products[0].invoice[b].buyer.shipping_address.City+', State: '+res_orders[a].Products[0].invoice[b].buyer.shipping_address.State+', Pin: '+res_orders[a].Products[0].invoice[b].buyer.shipping_address.Pincode+'.';
      htmlpage +='</address>'
      htmlpage +='</div>'
      htmlpage +='</div></div>'
      htmlpage +='<div class="row ordernos">'
      htmlpage +='<table class="table">'
      htmlpage +='<tr>'
      htmlpage +='<td style="font-size:20px;"><b>Invoice Number: </b><span id="invoice_id">'+res_orders[a].Products[0].invoice[b].invoice_id+'</span></td>';
      htmlpage +='<td style="float:right;font-size:20px;"><b>Invoice Date : </b><span class="orderdate">'+moment(res_orders[a].created_at).format("DD/MM/YYYY")+'</span></td>';
      htmlpage +='</tr>';
      htmlpage +='</table>'
      htmlpage +='</div>'
      htmlpage +='<div class="row">'
      htmlpage +='<div class="col-12 table-responsive">'
       var htmlpage1 ='<table class="table table_invoice table-striped" style="font-size:9px;">'
       htmlpage1 +='<thead>'
       htmlpage1 +='<tr>'
       htmlpage1 +='<th>#</th>'
       htmlpage1 +='<th>Item ID</th>'
       htmlpage1 +='<th>HSN Code</th>'
       htmlpage1 +='<th>Item Description</th>'
       htmlpage1 +='<th>Unit Price <i class="fa fa-inr" style="font-size:9px;"></th>'
       htmlpage1 +='<th class="text-right">Qty</th>';
       if (state == true) {
         htmlpage1 +='<th class="text-right">CGST %</th>'
         htmlpage1 +='<th class="text-right">SGST %</th>'
       }else if (other_state == true) {
         htmlpage1 +='<th class="text-right" colspan=2>IGST %</th>'
       }else {
          htmlpage1 +='<th class="text-right">CGST %</th>'
          htmlpage1 +='<th class="text-right">UTGST %</th>'
       }
       htmlpage1 +='<th class="text-right">Total Tax <i class="fa fa-inr" style="font-size:9px;"></th>'
       htmlpage1 +='<th class="text-right">Total Amount <i class="fa fa-inr" style="font-size:9px;"></th>'
       htmlpage1 +='</tr>'
       htmlpage1 +='</thead>'
       htmlpage1 +='<tbody id="orderitems_invoice">'
       htmlpage1 +='</tbody>'
       htmlpage1 +='<tfoot>'
       htmlpage1 +='<tr style="border:2px solid #ddd;">'
       htmlpage1 +='<th style="width:8%;"> Total <i class="fa fa-inr" style="font-size:9px;"></i></th>'
       htmlpage1 +='<th></th>'
       htmlpage1 +='<th></th>'
       htmlpage1 +='<th></th>'
       htmlpage1 +='<th class="text-right"></th>'
       htmlpage1 +='<th class="text-right"></th>'
       htmlpage1 +='<th class="text-right"></th>'
       htmlpage1 +='<th class="text-right"></th>'
       htmlpage1 +='<th class="text-right" ><span class="alltaxprice1" id="all_items_tax"></span></th>'
       htmlpage1 +='<th class="text-right"><span id="all_items_price"></span></th>'
      htmlpage1 +='</tr>'
      htmlpage1 +='</tfoot>'
      htmlpage1 +='</table>'
      var htmlpage2 ='</div>'
      htmlpage2 +='</div>'
    htmlpage2 +='</div>'
    htmlpage2 +='</div><br>';
    htmlpage2 +='<div class="container"><p>Note : This is a computer generated document. It does not require signature/Seal. </p></div>';
    htmlpage2 +='</section>';
    //var grandtotal = '';
    var items=res_orders[a].Products[0].invoice[b].orderItems;
    var grandtotaltax_cgst = 0;
    var grandtotaltax_sgst = 0;
    var grandtotaltax_igst = 0;
    var grandtotaltax_ugst = 0;
    var grandtotaltaxamount=0;
    var grandtotal_cost = 0;
    var grandtotal_shipping = 0;
    var grandtotal_shipping_gst = 0;
    var noofpages = 1;
      if(items.length>5){
        var gsgsgs5 = '';
        var len = items.length;
        //noofpages = len / 5;
        noofpages = Math.floor(len/5);
        if ((len % 5) > 0) {
          noofpages += 1;
        }
          for (var i = 1; i <= noofpages; i++) {
            var htmlpagetable ='<table class="table table_invoice table-striped" style="font-size:9px;">'
            htmlpagetable +='<thead>'
            htmlpagetable +='<tr>'
            htmlpagetable += '<th>#</th>'
            htmlpagetable +='<th>Item ID</th>'
            htmlpagetable +='<th>HSN Code</th>'
            htmlpagetable +='<th>Item Description</th>'
            htmlpagetable +='<th>Unit Price <i class="fa fa-inr" style="font-size:9px;"></i></th>'
            htmlpagetable +='<th class="text-right">Qty</th>';
            if (state == true) {
              htmlpagetable +='<th class="text-right">CGST %</th>'
              htmlpagetable +='<th class="text-right">SGST %</th>'
            }else if (other_state == true) {
              htmlpagetable +='<th class="text-right" colspan=2>IGST %</th>'
            }else {
               htmlpagetable +='<th class="text-right">CGST %</th>'
               htmlpagetable +='<th class="text-right">UTGST %</th>'
            }
          htmlpagetable +='<th class="text-right">Total Tax <i class="fa fa-inr" style="font-size:9px;"></i></th>'
          htmlpagetable +='<th class="text-right">Total Amount <i class="fa fa-inr" style="font-size:9px;"></i></th>'
          htmlpagetable +='</tr>'
          htmlpagetable +='</thead>'
          htmlpagetable +='<tbody id="orderitems_invoice" class="orderitems_invoicepage'+i+'">'
          htmlpagetable +='</tbody>'
           htmlpagetable +='</tfoot>';
           htmlpagetable +='<tr style="border:2px solid #ddd;">'
           htmlpagetable +='<th style="width:8%;">Total <i class="fa fa-inr" style="font-size:9px;"></i></th>'
           htmlpagetable +='<th></th>'
           htmlpagetable +='<th></th>'
           htmlpagetable +='<th></th>'
           htmlpagetable +='<th class="text-right"></th>'
           htmlpagetable +='<th class="text-right"></th>'
           htmlpagetable +='<th class="text-right"></th>'
           htmlpagetable +='<th class="text-right"></th>'
           htmlpagetable +='<th class="text-right"><span class="totaltaxperpage'+i+'" id="all_items_tax'+i+'"></span></th>'
           htmlpagetable +='<th class="text-right"><span class="totalpriceperpage'+i+'" id="all_items_price'+i+'"></span></th>'
           htmlpagetable +='</tr>'
           htmlpagetable +='</table>';

           htmlpagetable += '<div class="container" style="width:auto;height:165px;background-color:white;" id="allTotalspagedisp'+i+'">';
           htmlpagetable +='</div>';
           htmlpagetable += '<div style="display:none;" class="allTotalspage" id="allTotalspage'+i+'">';
           htmlpagetable += '<div style="text-align:right;">';
           htmlpagetable += '<p>Total Amount <i class="fa fa-inr" style="font-size:9px;"></i>  :  <span class="Tnetamt"></span></p>';
           htmlpagetable += '<p>Total GST Amount <i class="fa fa-inr" style="font-size:9px;"></i>  :  <span class="TotalGST_amt"></span></p>';
           htmlpagetable += '<p>Shipping Charges <i class="fa fa-inr" style="font-size:9px;"></i>  :   <span class="Shipping_amt"></span> </p>';
           htmlpagetable += '<p style="font-size: 12px;">Shipping GST <i class="fa fa-inr" style="font-size:9px;"></i> :   <span class="Shipping_tax"></span> </p>';
           htmlpagetable += '</div>';
           htmlpagetable += '<div style="text-align:right;">';
           htmlpagetable += '<div class="total-payment"><h3><b>Grand Total <i class="fa fa-inr"></i> :</b> <span class="Gtotal"></span></h3><p id="totalinwords"></p></div>';
           htmlpagetable += '</div></div>';
            gsgsgs5 += htmlpage+htmlpagetable+htmlpage2;
          }
          $("#pagess").html(gsgsgs5);
          for (var j = 1; j<=noofpages; j++ ) {
            var msg5k='';
            var start = (j-1)*5;
            var end = start+5;
          if (end > items.length) {
            end = items.length;
          }
          var totaltax_cgst=0;
          var totaltax_sgst=0;
          var totaltax_igst=0;
          var totaltax_shipping_cgst=0;
          var totaltax_shipping_sgst=0;
          var totaltax_shipping_igst=0;
          var totalgsttaxamount=0;
          var grandtotal_tax=0;
          var totalcost = 0;
          var total_shipping = 0;
          var total_shipping_gst = 0;

          for (var i = start; i < end; i++) {
              msg5k+='<tr><td>'+(i+1)+'</td>';
              msg5k+='<td>'+items[i].product_id+'</td>';
              if(items[i].hsn_code == undefined){
                var hsn = "Not Applicable";
              }else{
                var hsn = items[i].hsn_code;
              }
              msg5k+='<td>'+hsn+'</td>';
              msg5k+='<td>'+items[i].product_name+'</td>';
              msg5k+='<td class="text-right">'+items[i].price+'</td>';
              msg5k+='<td class="text-right">'+items[i].qty+'</td>';
              if (state == true) {
                msg5k+='<td class="text-right">'+items[i].cgst_amt+' ('+items[i].cgst_pct+')</td>';
                msg5k+='<td class="text-right">'+items[i].sgst_amt+' ('+items[i].sgst_pct+')</td>';
              }else if (other_state == true) {
                msg5k+='<td class="text-right" colspan=2>'+items[i].igst_amt+' ('+items[i].igst_pct+' %)</td>';
              }else {
               msg5k+='<td class="text-right">'+items[i].cgst_amt+' ('+items[i].cgst_pct+')</td>';
               msg5k+='<td class="text-right">'+items[i].utgst_amt+' ('+items[i].utgst_pct+')</td>';
              }

              totaltax_cgst += items[i].cgst_amt;
              totaltax_sgst += items[i].sgst_amt;
              totaltax_igst += items[i].igst_amt;
              var totaltax_amt =items[i].cgst_amt+items[i].sgst_amt+items[i].igst_amt+items[i].utgst_amt;
              totalshipping = items[i].shipping_charges;
              grandtotal_shipping += items[i].shipping_charges;
              totaltax_shipping_cgst += items[i].shipping_cgst_amt;
              totaltax_shipping_sgst += items[i].shipping_sgst_amt;
              totaltax_shipping_igst += items[i].shipping_igst_amt;
              total_shipping_gst = totalshipping+totaltax_shipping_cgst + totaltax_shipping_sgst + totaltax_shipping_igst;
              totalcost+=totaltax_amt+parseFloat(items[i].price)*items[i].qty;
              totalgsttaxamount += totaltax_amt;
              msg5k+='<td class="text-right">'+totaltax_amt+'</td><td class="text-right">'+totalcost.toFixed(2)+'</td></tr>';
          }
          grandtotaltax_cgst += totaltax_cgst;
          grandtotaltax_sgst += totaltax_sgst;
          grandtotaltax_igst += totaltax_igst;
          grandtotaltaxamount += totalgsttaxamount;
          grandtotal_cost += totalcost;
          grandtotal_shipping += total_shipping;
          grandtotal_shipping_gst += total_shipping_gst;
          $(".totalpriceperpage"+j).html(totalcost.toFixed(2));
          $(".totaltaxperpage"+j).html(totalgsttaxamount.toFixed(2));
          $(".totalshippingperpage"+j).html(total_shipping.toFixed(2));
          $(".totalshippingtaxperpage"+j).html(total_shipping_gst.toFixed(2));
          $('.orderitems_invoicepage'+j).html(msg5k);
        }
      }else{
        var msg='';
        var gsgsgs = '';
        var totaldiv = '<div class="" id="allTotalspage11"><div style="text-align:right;"><p>Total Amount <i class="fa fa-inr" style="font-size: 9px;"></i>  :  <span class="Tnetamt"></span></p>';
        totaldiv += '<p>Total GST Amount <i class="fa fa-inr" style="font-size: 9px;"></i>  :  <span class="TotalGST_amt"></span></p>';
        totaldiv += '<p>Shipping Charges <i class="fa fa-inr" style="font-size: 9px;"></i>  :   <span class="Shipping_amt"></span> </p><p style="font-size: 12px;">Shipping GST <i class="fa fa-inr" style="font-size: 9px;"></i> :   <span class="Shipping_tax"></span> </p>';
        totaldiv += '</div>';
        totaldiv += '<div style="text-align:right;"><div class="total-payment"><h3><b>Grand Total <i class="fa fa-inr"></i> :</b> <span class="Gtotal1"></span></h3><br><p id="totalinwords"></p></div></div> ';
        gsgsgs += htmlpage+htmlpage1+totaldiv+htmlpage2;
        var grandtotal_tax1 = 0;
        for (var i = 0; i < items.length; i++) {
          msg+='<tr><td>'+(i+1)+'</td>';
          msg+='<td>'+items[i].product_id+'</td>';
          if(items[i].hsn_code == undefined){
            var hsn = "Not Applicable";
          }else{
            var hsn = items[i].hsn_code;
          }
          msg+='<td>'+hsn+'</td>';
          msg+='<td>'+items[i].product_name+'</td>';
          msg+='<td class="text-right"></i>'+items[i].price.toFixed(2)+'</td>';
          msg+='<td class="text-right">'+items[i].qty+'</td>';
          if (state == true) {
            msg+='<td class="text-right">'+items[i].cgst_amt+' ('+items[i].cgst_pct+')</td>';
            msg+='<td class="text-right">'+items[i].sgst_amt+' ('+items[i].sgst_pct+')</td>';
          }else if (other_state == true) {
            msg+='<td class="text-right" colspan=2>'+items[i].igst_amt+' ('+items[i].igst_pct+' %)</td>';
          }else {
             msg+='<td class="text-right">'+items[i].cgst_amt+' ('+items[i].cgst_pct+')</td>';
             msg+='<td class="text-right">'+items[i].utgst_amt+' ('+items[i].utgst_pct+')</td>';
          }
          var totaltax_per=items[i].cgst_pct+items[i].sgst_pct+items[i].igst_pct+items[i].utgst_pct;
          var totaltax_amt=items[i].cgst_amt+items[i].sgst_amt+items[i].igst_amt+items[i].utgst_amt;
          var totalcost=totaltax_amt+parseFloat(items[i].price)*items[i].qty;
          msg+='<td class="text-right"></i>'+totaltax_amt+'</td><td class="text-right"></i>'+totalcost.toFixed(2)+'</td></tr>'
          grandtotaltax_cgst += items[i].cgst_pct;
          grandtotaltax_sgst += items[i].sgst_pct;
          grandtotaltax_igst += items[i].igst_pct;
          grandtotaltax_ugst += items[i].utgst_amt;
          grandtotal_cost += totalcost;
          grandtotal_tax1 += totaltax_amt;
          grandtotal_shipping += items[i].shipping_charges;
          shipping_tax =  items[i].shipping_cgst_amt+items[i].shipping_igst_amt+items[i].shipping_sgst_amt+items[i].shipping_utgst_amt;
        }
        $("#pagess").html(gsgsgs);
        $('#orderitems_invoice').html(msg);
      }
      var cls_btn='';
      var btn_name='';
      var del_status=0;
      var del_latest=res_orders[a].Products[0].invoice[b].delivery_status.length-1;
      if (res_orders[a].Products[0].invoice[b].delivery_status[del_latest].Review=='No'&&res_orders[a].Products[0].invoice[b].delivery_status[del_latest].Dispatch=='No') {
        cls_btn='btn-danger';
        btn_name='Mark as Reviewed';
        del_status=1;
      }else if (res_orders[a].Products[0].invoice[b].delivery_status[del_latest].Review=='Yes'&&res_orders[a].Products[0].invoice[b].delivery_status[del_latest].Dispatch=='No') {
        cls_btn='btn-warning';
        btn_name='Mark as Dispatched';
        del_status=2;
      }else if (res_orders[a].Products[0].invoice[b].delivery_status[del_latest].Review=='Yes'&&res_orders[a].Products[0].invoice[b].delivery_status[del_latest].Dispatch=='Yes') {
        cls_btn='btn-success';
        btn_name='Mark as Return';
        del_status=3;
      }
    $('.to_review').attr('onclick','change_stage('+b+','+del_status+')');
    $('.to_review').addClass(cls_btn);
    $('#to_review_txt').text(btn_name);
    $('.Shipping_amt').text(grandtotal_shipping.toFixed(2));
    var shippingtax = grandtotal_shipping*0.18
    $('.Shipping_tax').text(shippingtax.toFixed(2));
    if (grandtotal_tax1==undefined) {
      $('.alltaxprice1').text(grandtotal_tax1);
    }else {
      $('.alltaxprice1').text(grandtotal_tax1.toFixed(2));
    }
    $('.Gtotal1').text((grandtotal_cost+grandtotal_shipping+shippingtax).toFixed(2));
    $('#all_items_price').text((grandtotal_cost-grandtotal_tax1).toFixed(2));
    $('.Tnetamt').text((grandtotal_cost-grandtotal_tax1).toFixed(2));
    $('.TotalGST_amt').text(grandtotal_tax1.toFixed(2));
    $('.Gtotal').text((grandtotal_cost+grandtotal_shipping+shippingtax).toFixed(2));
    $("#totalinwords").html(res_orders[a].Products[0].invoice[b].TotalAmountInWords)
    $('#myModal_invoice').modal('show');
    $("#allTotalspage"+noofpages).show();
    $("#allTotalspagedisp"+noofpages).hide();
  }
