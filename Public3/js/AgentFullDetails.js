
function GetDetails(){
var urldata = JSON.parse('{"' + decodeURI(location.search.substring(1).replace(/%2C/g, ",").replace(/&/g, "\",\"").replace(/%23/g, "#").replace(/=/g, "\":\"") + '"}'));

data = {};
data.id = urldata.aid;
$.ajax({
    data : data,
    dataType : 'json',
    type : 'POST',
    url : '/getsingleagentwallet',
    success : function (response) {
      $("#walletbal").html(response[0].Wallet.toFixed(2));
    }
});
$.ajax({
    data : data,
    dataType : 'json',
    type : 'POST',
    url : '/AgentProfileDetails1',
    success : function (response) {
      console.log(response);
      $("#FullName").html(response.UserName);
      $("#AgentName").html(response.UserName);
      $("#AgentEmail").html(response.email);
      $("#AgentMobile").html(response.Mobile);
      if(response.Aadhar == "" || response.PANCard == "" || response.Photo == ""){
        $("#kycstatus").show();
        $("#getkycss").hide();
      }else{
        $("#get_aadhaar").attr('href','Public2/images/EImages/'+response.Aadhar);
        $("#get_pan").attr('href','Public2/images/EImages/'+response.PANCard);
        $("#get_photo").attr('href','Public2/images/EImages/'+response.Photo);
      }
    }
  });
  $.ajax({
        data : data,
        dataType : 'json',
        type : 'POST',
        url : '/AgentECommerceReports',
        success : function (response) {
            var msg = '';
            $.each(response,function(i,v){
              msg += '<tr><td>'+v.BillNumber+'</td><td>'+v.ProductName+'</td><td>'+v.Amount+'</td><td>';
              if(v.Status == 1){
                msg += 'Not Delivered';
              }
              else{
                msg += 'Delivered';
              }
              msg +='</td><td>'+moment(v.OrderDate).format('LLL')+'</td><td>'+v.Quantity+'</td><td>'+v.Quantity+'</td>'+v.SkuNumber+'</tr>';
            })
          $("#agentecreports").html(msg);
        }
    });

  $.ajax({
      data : data,
      dataType : 'json',
      type : 'POST',
      url : '/AgentDMTReports',
      success : function (response) {
          var msg = '';
          $.each(response,function(i,v){
            msg += '<tr><td>NHR123456</td><td>1500 Rs</td><td>AC123456</td><td>SBI / 23456</td><td>04-12-2017</td><td>Ramu</td></tr>';
          })
        $("#agentdmtreports").html(msg);
      }
  });
}

function agentsdata(){
$('#example1').dataTable({
  processing: true,
  destroy:true,
  autoWidth:false,
  jQueryUI: true,
  sAjaxDataProp: 'data',
    ajax:{
    url: "/getStudents",
    type: 'POST',
    data: '',
    dataSrc: ""
    },
    columns: [
      {
      data: "_id",
      className: "center",
      render: function(data, type, row, meta){
            if(type === 'display'){
              data='<td><a href="/AgentFullDetails?aid='+row._id+'">'+row.UserName+'</a></td>'
          }
        return data;
      }
  },
    { data : "Mobile" },
    { data : "email" },
    {
        data: "_id",
        className: "center",
        render: function(data, type, row, meta){
              if(type === 'display'){
                  data='<td><div class="row"><div class="col-sm-5"><Button onclick = PanOutlet("'+encodeURIComponent(row._id)+'","'+encodeURIComponent(row.UserName)+'","'+encodeURIComponent(row.Mobile)+'","'+encodeURIComponent(row.email)+'","'+encodeURIComponent(row.City)+'","'+encodeURIComponent(row.State)+'","'+encodeURIComponent(row.PinCode)+'") >Create</Button></div></td>';
            }
          return data;
        }
    },
    {
              data: "_id",
         className: "center",
         render: function(data, type, row, meta){
                if(type === 'display'){
                      data=  '<div class="col-sm-5"><Button onclick = utilogin("'+data+'") >Create</Button></div></td>';
                  }
                return data;
             }
    },
       {
         data:"_id",
     // className: ,
         render: function(data, type, row, meta){
          // console.log(meta);
             if(type === 'display'){
                //  data= '<td tableX.button(0).nodes().style=color:green;>Created</td>';
                if(row.RemitterId == "0" || row.RemitterId == null || row.RemitterId == undefined){
                    data = '<td><div class="row"><div class="col-sm-5"><a onclick = DMTRemitter("'+row._id+'","'+encodeURIComponent(row.UserName)+'","'+row.Mobile+'","'+row.PinCode+'") >Create</a></div></td>';
                   }else{
                    data = '<td><span style="color:green;">Created</span></td>';
                   }
              }
                return data;
             }
    },
    {
              data: "_id",
         className: "center",
         render: function(data, type, row, meta){
                if(type === 'display'){
                  data ='<td><div class="row"><div class="col-sm-5"><a onclick = Editagent("'+data+'") >Edit</a></div><div class="col-sm-7"><i class="fa fa-remove text-red fa-2x" onclick = deleteagent("'+data+'")></i></div></div></td></tr>';
                }
                return data;
    }
    },
    ],
});
}
$("#Add_Agent_Form").submit(function (e) {
    e.preventDefault();
    var AgentId = $("#Agentid").val();
    $('.notifyjs-wrapper').trigger('notify-hide');
    if(!checkInputAlpha($('#fname').val().trim()) || $('#fname').val().trim() == ''){
        $('#fname').focus().notify("Please Enter First Name...", { className: "error", position:"bottom" });
        return false;
    }
    if (AgentId == "") {
      if(!checkInputAlpha($('#lname').val().trim()) || $('#lname').val().trim() == ''){
        $('#lname').focus().notify("Please Enter Last Name...", { className: "error", position:"bottom" });
        return false;
      }
    }

    if(!checkInputEmail($('#agent_email').val().trim())){
        $('#agent_email').focus().notify("Please Enter Valid Email...", { className: "error", position:"bottom" });
        return false;
    }
    //var mob= $('#mobilenum').val();
    if($('#mobilenum').val() == ""){
        $('#mobilenum').focus().notify("Please Enter Mobile No...", { className: "error", position:"bottom" });
        return false;
    }
    if($('#address').val().trim() == ''){
        $('#address').focus().notify("Please Enter Address...", { className: "error", position:"bottom" });
        return false;
    }
    if(!checkInputAlpha($('#city').val().trim()) || $('#city').val().trim() == ''){
        $('#city').focus().notify("Please Enter City...", { className: "error", position:"bottom" });
        return false;
    }
    if($('#Street').val().trim() == ''){
        $('#Street').focus().notify("Please Enter Street Name...", { className: "error", position:"bottom" });
        return false;
    }
    if($('#Village').val().trim() == ''){
        $('#Village').focus().notify("Please Enter Village...", { className: "error", position:"bottom" });
        return false;
    }
    if(!checkInputAlpha($('#state').val().trim()) || $('#state').val().trim() == ''){
        $('#state').focus().notify("Please Enter State...", { className: "error", position:"bottom" });
        return false;
    }
    if(!checkInputNum($('#zipcode').val().trim()) || $('#zipcode').val().trim().length < 5){
        $('#zipcode').focus().notify("Please Enter Zip Code...", { className: "error", position:"bottom" });
        return false;
    }
    if($('#pannumber').val().trim() == ''){
        $('#pannumber').focus().notify("Please Enter Pan Number...", { className: "error", position:"bottom" });
        return false;
    }
    if($('#shippingdoornumber').val().trim() == ''){
        $('#shippingdoornumber').focus().notify("Please Enter Shipping Door Number...", { className: "error", position:"bottom" });
        return false;
    }
    if($('#shippingstreet').val().trim() == ''){
        $('#shippingstreet').focus().notify("Please Enter Shipping Street...", { className: "error", position:"bottom" });
        return false;
    }
    if($('#shippingcity').val().trim() == ''){
        $('#shippingcity').focus().notify("Please Enter Shipping City...", { className: "error", position:"bottom" });
        return false;
    }
    if($('#shippingstate').val().trim() == ''){
        $('#shippingstate').focus().notify("Please Enter Shipping State...", { className: "error", position:"bottom" });
        return false;
    }
    if($('#shippingpincode').val().trim() == ''){
        $('#shippingpincode').focus().notify("Please Enter Shipping Pin Code...", { className: "error", position:"bottom" });
        return false;
    }
    if($('#billingdoornumber').val().trim() == ''){
        $('#billingdoornumber').focus().notify("Please Enter Billing Door Number...", { className: "error", position:"bottom" });
        return false;
    }
    if($('#billingstreet').val().trim() == ''){
        $('#billingstreet').focus().notify("Please Enter Billing Street...", { className: "error", position:"bottom" });
        return false;
    }
    if($('#billingcity').val().trim() == ''){
        $('#billingcity').focus().notify("Please Enter Billing City...", { className: "error", position:"bottom" });
        return false;
    }
    if($('#billingstate').val().trim() == ''){
        $('#billingstate').focus().notify("Please Enter Billing State...", { className: "error", position:"bottom" });
        return false;
    }
    if($('#billingpincode').val().trim() == ''){
        $('#billingpincode').focus().notify("Please Enter Billing Pin Code...", { className: "error", position:"bottom" });
        return false;
    }
    var formdata = $(this).serializeArray();
    if(AgentId == ""){
      var url = '/AddAgent';
    }
    else{
      var url = '/EditAgent';
    }
    $.ajax({
        url : url,
        type : 'POST',
        data : formdata,
        dataType : 'JSON',
        success : function(res){
          if(res == "0"){
            $('#addagentbtn').focus().notify("Email Already Exist...", { className: "error", position:"bottom" });
          }
          else if (res=='12') {
            $('#addagentbtn').focus().notify("Business Partner Updated Successfully", { className: "success", position:"bottom" });
            setTimeout(function(){
                $('#addagentmodal').modal('hide');
                agentsdata();
            }, 700);
          }else {
            $('#addagentbtn').focus().notify("Business Partner Added Successfully", { className: "success", position:"bottom" });
            setTimeout(function(){
                $('#addagentmodal').modal('hide');
                agentsdata();
            }, 700);
          }
        }
    })
});
function deleteagent(id){
  if(confirm("Are you sure you want to delete this?")){
    data = {};
    data.aid = id;
    $.ajax({
                data : data,
                dataType : 'json',
                type : 'POST',
                url : '/DeleteAgent',
                success : function (response) {
                    if(response == "1"){
                      location.reload();
                    }else{
                      alert("Not deleted..")
                      return false;
                    }
                }

      });
    }
    else{
        return false;
    }


}

$('.kyc_regis').submit(function(e) {
  e.preventDefault();
  var Photo = $('#photoupload')[0].files[0];
  var PanCard = $('#PANupload')[0].files[0];
  var Aadhar = $('#Aadharupload')[0].files[0];
  var agentid=$("#Agentid").val();
  var formData = new FormData();
      formData.append('Photo', Photo);
      formData.append('PanCard',PanCard);
      formData.append('Aadhar',Aadhar);
      formData.append('agentid',agentid)
      $.ajax({
          async      : false,
          cache      : false,
          contentType: false,
          data       : formData,
          processData: false,
          type       : 'POST',
          url        : '/kyc_editAgent',
          success    : function (res) {
            if(res == 1){
              $('#kyc_regisbtn').notify("Document uploaded...", { className: "success", position:"right" });
              setTimeout(function(){ $("#addagentmodal").modal("hide") }, 2000);

            }
            else{
              $('#kyc_regisbtn').notify("Error in upload...", { className: "error", position:"bottom" });
            }
          }
      })
})

// AQAPY5706B
function Editagent(id){
  $("#addagentmodal").modal("show");
  $("#lastnameagent").hide();
  $("#uid_div").show();
  data = {};
  data.id = id;
  $.ajax({
        data : data,
        dataType : 'json',
        type : 'POST',
        url : '/AgentProfileDetails1',
        success : function (res) {
          $("#Agentid").val(id);
          $("#fname").val(res.UserName);
      //  $("#lname").val(res.UserName);
          $("#agent_email").val(res.email);
          $("#mobilenum").val(res.Mobile);
          $("#address").val(res.HNo);
          $("#city").val(res.City);
          $("#Street").val(res.Street);
          $("#Village").val(res.Village);
          $("#state").val(res.State);
          $("#zipcode").val(res.PinCode);
          $("#pannumber").val(res.PanNumber);
          $("#gstnumber").val(res.GSTNumber);
          $("#shippingdoornumber").val(res.Shipping_DoorNumber);
          $("#shippingstreet").val(res.Shipping_Street);
          $("#shippingcity").val(res.Shipping_City);
          $("#shippingstate").val(res.Shipping_State);
          $("#shippingpincode").val(res.Shipping_PinCode);
          $("#billingdoornumber").val(res.Billing_DoorNumber);
          $("#billingstreet").val(res.Billing_Street);
          $("#billingcity").val(res.Billing_City);
          $("#billingstate").val(res.Billing_State);
          $("#billingpincode").val(res.Billing_PinCode);
        }
    });
}
function PanOutlet(id,name,mobile,email,City,State,PinCode){
  $("#Agentidpan").val(id);
  $("#AgentId").val(id);
  $("#OutLetName").val(decodeURIComponent(name));
  $("#MobileNumber").val(mobile);
  $("#OutLetMobileNumber").val(mobile);
  $("#OutLetEmail").val(decodeURIComponent(email));
  $("#OutLetAddress").val(decodeURIComponent(City)+", "+decodeURIComponent(State));
  $("#OutLetPinCode").val(PinCode);
  $("#RegisterOutletModal").modal("show");
  return false;
}

$("#Outform1").submit(function(){
  // $("#Outletfirsttab").hide();
  // $("#Outlet1").show();
  // return false;
  var data = {};
  data.MobileNumber = $("#MobileNumber").val();
  if(data.MobileNumber == ""){
    $("#MobileNumber").notify("Enter Mobile Number", { className: "error", position:"right" });
    return false;
  }
  $.ajax({
      type: 'POST',
      data: JSON.stringify(data),
      contentType: 'application/json',
      url: '/VerifyOutlet',
      async:false,
      crossDomain:true,
      success: function(res) {
        if(res.statuscode == "TXN"){
          $("#Outletfirsttab").hide();
          $("#Outlet1").show();
          return false;
        }
        else{
          $("#MobileNUmberSubmit").notify("Please Try Again..", { className: "error", position:"left" });
          return false;
        }
      }
  });
  return false;
})

$("#RegisterOutLetForm").submit(function(e){
    // $("#Outletfirsttab").hide();
    // $("#Outlet1").hide();
    // $("#Outlet2").show();
    // return false;
    e.preventDefault();
    var data = {};
    data.mobile = $("#OutLetMobileNumber").val();
    data.otp = $("#OutLetOTP").val();
    data.Email = $("#OutLetEmail").val();
    data.StoreType = $("#OutLetStoreType").val();
    data.Company = $("#OutLetCompany").val();
    data.Name = $("#OutLetName").val();
    data.Address = $("#OutLetAddress").val();
    data.PinCode = $("#OutLetPinCode").val();
    data.pannumber = $("#PanNumber").val();
    $.ajax({
      type: 'POST',
      data: JSON.stringify(data),
      contentType: 'application/json',
      url: '/RegisterOulet',
      async:true,
      crossDomain:true,
        success: function(res) {
          if(res.statuscode == "TXN"){
            $("#outletid").val(res.data.outlet_id);
            $("#upoutletid").val(res.data.outlet_id);
            $("#Outlet1").hide();
            //$("#Outlet2").show();
            $("#PanUpdate").show();
          }
          else{
            console.log("Error");
          }
        }
    });
    return false;
});

$("#updatepanform").submit(function(e){
    e.preventDefault();
    var data = {};
    data.agentid = $("#Agentidpan").val();
    data.outletId = $("#upoutletid").val();
    data.pannumber = $("#updatePanNumber").val();
    $.ajax({
      type: 'POST',
      data: JSON.stringify(data),
      contentType: 'application/json',
      url: '/Getkyc',
      async:true,
      crossDomain:true,
        success: function(res) {
          console.log(res);
          if(res.statuscode == "ERR"){
            alert("Can not Update Pan Number..Please contact InstantPay ")
            return false;
          }else if(res.statuscode == "TXN"){
            var data1 = {};
            data1.agentid = data.agentid;
            data1.outletdisdp = data.outletId;
            data1.pannumberdp = data.pannumber;
            data1.panid = res.data.REQUIRED[0][0];
            data1.adarid = res.data.REQUIRED[1][0];
            $.ajax({
              type: 'POST',
              data: JSON.stringify(data1),
              contentType: 'application/json',
              url: '/UploadDocument',
              async:true,
              crossDomain:true,
                success: function(res1) {
                  console.log(res1);
                  if (res1.statuscode == "TXN") {
                    alert("uploaded successfully.Please go through instantpay dashboard and check status")
                  }else{
                    alert("Please try again");
                  }
                }
            });
          }
          // else if(res1.data.SCREENING[0][1] == "PAN Card"){
          //   alert("Documents uploaded");
          // }
          else{
            alert("Some thing error..Please contact developer");
          }

          // else if(res.statuscode == "TXN"){
          //   $("#PanID").val(res.data.REQUIRED[0][0]);
          //   $("#AdharID").val(res.data.REQUIRED[1][0]);
          //   $("#Outlet1").hide();
          //   $("#PanUpdate").hide();
          //   $("#Outlet2").show();
          // }
        }
    });
    return false;
});



function encodeImageFileAsURLPan(element,id) {
  var pan = $("#repannumber").val();
  if(pan == ""){
    $("#repannumber").notify("Please Enter pan number..", { className: "error", position:"right" });
    return false;
  }
  $("#inprogress1").show();
  var file = element.files[0];
  var reader = new FileReader();
  reader.onloadend = function() {
    //console.log('RESULT', reader.result)
  }
  reader.readAsDataURL(file);
  var data = {};
  data.docid = $("#PanID").val();;
  data.outletid = $("#outletid").val();
  data.pannumber = $("#repannumber").val();
  data.filename = file.name;
  data.imagecode = 'RESULT', reader.result;
  $.ajax({
    type: 'POST',
    data: JSON.stringify(data),
    contentType: 'application/json',
    url: '/UploadDocument',
    async:true,
    crossDomain:true,
      success: function(res) {
        console.log(res);
        if(res.statuscode == "TXN"){
          $("#done1").show();
          $("#inprogress1").hide();
        }
        else{
          console.log("ERROR");
        }
      }
  });
}
function encodeImageFileAsURLAdar(element,id) {
  var pan = $("#repannumber").val();
  if(pan == ""){
    $("#repannumber").notify("Please Enter pan number..", { className: "error", position:"right" });
    return false;
  }
  $("#inprogress2").show();
  var file = element.files[0];
  var reader = new FileReader();
  reader.onloadend = function() {
    //console.log('RESULT', reader.result)
  }
  reader.readAsDataURL(file);
  var data = {};
  data.docid = $("#PanID").val();;
  data.outletid = $("#outletid").val();
  data.pannumber = $("#repannumber").val();
  data.filename = file.name;
  data.imagecode = 'RESULT', reader.result;
  $.ajax({
    type: 'POST',
    data: JSON.stringify(data),
    contentType: 'application/json',
    url: '/UploadDocument',
    async:true,
    crossDomain:true,
      success: function(res) {
        console.log(res);
        if(res.statuscode == "TXN"){
          $("#done2").show();
          $("#inprogress2").hide();
        }
        else{
          console.log("ERROR");
          //location.reload();
        }

      }
  });
}
function uniqnum(pan){
  var text = "";
  var possible = pan;
  for( var i=0; i < 5; i++ )
      text += possible.charAt(Math.floor(Math.random() * possible.length));
    return text;
}

function DMTRemitter(id,name,mobile,pin){
  var en_id=decodeURIComponent(id);
  var en_name=decodeURIComponent(name);
  var en_mobile=decodeURIComponent(mobile);
  var en_pin=decodeURIComponent(pin);
  $("#RegisterRemitterModal").modal("show");
  $("#agentid").val(en_id);
  $("#agentid1").val(en_id);
  $("#usernameremitter").val(en_name);
  $("#mobilenumberemitter").val(en_mobile);
  $("#pincodermter").val(en_pin);
}
function utilogin(agentid){
  $("#UTIlogindetails").modal("show");
  $("#agentidforuti").val(agentid);
  return false;
}

$("#getutiloginbtn").click(function(){
  var data = {};
  data.aid = $("#agentidforuti").val();
  $.ajax({
      url : '/PanServcieAgent',
      type : 'POST',
      data : data,
      dataType : 'JSON',
      success : function(res){
        console.log(res);
          if(res == "0"){
            $("#getutiloginbtn").notify("Complete KYC..", { className: "error", position:"right" });
            return false;
          }else if(res == "5"){
            $("#getutiloginbtn").notify("Please create Outlet first...", { className: "error", position:"right" });
            return false;
          }
          else if(res == "1"){
          $("#getutiloginbtn").notify("Uti registered successfully", { className: "success", position:"right" });
          setTimeout(function(){ location.reload(); }, 5000);
          return false;
        }else{
          $("#utiloginid").html(res.utilodinid);
          $("#utiloginpassword").html(res.utilodinid);
        }
      }
  })
  return false;
})

$("#Add_Agent_Wallet_Form").submit(function (e) {
    e.preventDefault();
    $('.notifyjs-wrapper').trigger('notify-hide');
    var urldata = JSON.parse('{"' + decodeURI(location.search.substring(1).replace(/%2C/g, ",").replace(/&/g, "\",\"").replace(/%23/g, "#").replace(/=/g, "\":\"") + '"}'));
    var data={};
    data.agentid = urldata.aid;
    data.utrnumber = $("#utrnumber").val();
    if(!checkInputAlphaNum($('#utrnumber').val()) || data.utrnumber.length>16){
    $('#utrnumber').focus().notify("Please Enter Id...", { className: "error", position:"bottom" });
    return false;
    }
    data.dateandtime = $("#dateandtime").val();
    if(data.dateandtime == "" ){
        $("#dateandtime").notify("Please Select date", { className: "error", position:"down" });
        return false;
    }
    data.Amount = $("#agentWallet").val();
    if(!checkInputAmount($('#agentWallet').val())){
    $('#agentWallet').focus().notify("Please Enter Amount...", { className: "error", position:"bottom" });
    return false;
    }
    data.referencename = $("#referencename").val();
    if(!checkInputAlpha($('#referencename').val().trim()) || $('#referencename').val().trim() == ''){
      $('#referencename').focus().notify("Please Enter  Name...", { className: "error", position:"bottom" });
      return false;
    }
    $("#addagentwalletbtn").prop('disabled', true);
    $.ajax({
        url : '/agentWalletAmount',
        type : 'POST',
        data : data,
        dataType : 'JSON',
        success : function(res){
          console.log(res);
          if(res == "0"){
            $("#addagentwalletbtn").notify("Cannnot Update..", { className: "error", position:"right" });
            return false;
          }else if (res == "null") {
            $("#utrnumber").notify("Reference Id Mismatch...", { className: "error", position:"right" });
            $("#addagentwalletbtn").prop('disabled', false);
            return false;
          }
          else{
            $("#addagentwalletbtn").notify("Updated successfully", { className: "success", position:"right" });
            setTimeout(function(){
              location.reload();
            },3000);
            return false;
          }
        }
    })

})

$("#remitterregform").submit(function (e) {
    e.preventDefault();
    $('.notifyjs-wrapper').trigger('notify-hide');
    var data={};
    data.agentid = $("#agentid").val();
    data.username =  $("#nameofremmiter").val();
    data.nameofremmiter = $("#nameofremmiter").val();
    data.surnameofremmiter = $("#surnameofremmiter").val();
    data.mobilenumber = $("#mobilenumberemitter").val();
    data.pincode = $("#pincodermter").val();
    $.ajax({
        url : '/RemitterRegistration',
        type : 'POST',
        data : data,
        dataType : 'JSON',
        success : function(res){
          console.log(res);
          if (res == "0") {
            $("#remittersbt").notify("Outlet empty..", { className: "error", position:"right" });
          }
          else if(res.statuscode == "TXN"){
            $("#mobilenumberemitter1").val(data.mobilenumber)
            $("#emmiterid").val(res.data.remitter.id)
            $("#remitterreg_div").hide();
            $("#remittervalidatio_div").show();
          }
          else{
            $("#remittersbt").notify("Some thing went wrong", { className: "error", position:"right" });
            return false;
          }
        }
    })
})
$("#remittersbtvalbtn").click(function(){
  var data = {};
  data.agentid = $("#agentid1").val();
  data.otp_rem = $("#otp_remmiter").val();
  data.mobilenumber = $("#mobilenumberemitter1").val();
  data.remmiterid = $("#remmiterid").val();
  console.log(data);
  $.ajax({
      url : '/RemitterRegistration_validation',
      type : 'POST',
      data : data,
      dataType : 'JSON',
      success : function(res){
        if (res == "1") {
          alert("Remitter created successfully");
          location.reload();
        }else{
          alert("some thing went wrong");
        }
      }
  })
})





function encodeImageFile(element,id) {
var s=  $("#Agentid").val()
  var file = element.files[0];
  var reader = new FileReader();
    var data = {};
  reader.onloadend = function() {
    if (id=='PANupload') {
      data={PANupload:reader.result};
    }else if (id=='Aadharupload') {
      data={Aadharupload:reader.result};
    }else if (id=='photoupload') {
      data={photoupload:reader.result};
    }
    data.agentid=s;
    $.ajax({
      type: 'POST',
      data: JSON.stringify(data),
      contentType: 'application/json',
      url: '/kyc_editAgent123',
      async:true,
      crossDomain:true,
        success: function(res) {
          console.log(res);
          if (res=="0") {
            $('#'+id).focus().notify("upload error...", { className: "error", position:"bottom" });
          }else if (res=='1') {
            $('#'+id).notify("Successfully uploaded...", { className: "success", position:"bottom" });
          }
        }
    });

  }
  reader.readAsDataURL(file);
}
