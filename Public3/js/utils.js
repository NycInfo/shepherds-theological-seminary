
function checkPassword(str)
 {
	 // at least one number, one lowercase and one uppercase letter
	 // at least six characters
	 var re = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}/;
	 return re.test(str);
 }
function toTitleCase(str) {
	return str.replace(/\w\S*/g, function (txt) {
    	return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
	});
}

function priceFormat(price){
	if(price){
		var LP = price.toString();
		if(LP.length>=4){
			LP = LP.replace(/(\d)(?=(\d{3})+$)/g, '$1,');
		}
	} else {
		var LP = 0;
	}
	return LP;
}

function priceReduce(a,b){

	if(a!=''){
		if(parseInt(a)>parseInt(b)){
			var diff = parseInt(a)-parseInt(b);
        	var per = parseInt(diff)/parseInt(a)*100;
        	return {d:priceFormat(diff),p:per.toFixed(2)};
		} else {
			return {d:'',p:''};
		}
	} else {
		return {d:'',p:''};
	}
}

function checkInputAlpha(v,id){
	var email = v.trim();
	var pat = /^[A-Za-z\s]+$/;
	if(email.match(pat)){
		$('.notifyjs-wrapper').trigger('notify-hide');
		return true;
	} else {
		$('#'+id).val('').focus().notify("Enter alphabets only", { className: "error", position:"bottom", });
		return false;
    }
}

function checkInputAlphaNum(v,id){
	var email = v.trim();
	var pat=/^[a-zA-Z]+[A-Za-z0-9\s]+$/;
	if(email.match(pat) && Number(email) != 0){
		$('.notifyjs-wrapper').trigger('notify-hide');
		return true;
	} else {
		$('#'+id).val('').focus().notify("Enter alphanumerics only", { className: "error", position:"bottom", });
		return false;
    }
}


function checkInputEmail(v,id){
	var email = v.toLowerCase().trim();
	var pat=/^([a-zA-Z])+([a-zA-Z0-9_\.\-])+\@([a-zA-Z])+(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9\-]{2,4})$/g
	//var pat=/^[a-zA-Z]+[A-Za-z0-9._-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,3}$/;
	if(email.match(pat)){
		$('.notifyjs-wrapper').trigger('notify-hide');
		return true;
	} else {
		$('#'+id).val('').focus().notify("Enter correct email", { className: "error", position:"bottom" });
		return false;
    }
}

function checkInputPhNum(v,id){
	var d = v.trim();
	var pat=/^[0-9]+$/;
	if(d.charAt(1)==0 || !/^\(?(\d{3})\)?[- ]?(\d{3})[- ]?(\d{4})$/.test(d)){
		$('#'+id).val('').focus().notify("Enter correct phone no", { className: "error", position:"bottom", });
		return false;
	} else {
		$('.notifyjs-wrapper').trigger('notify-hide');
		return true;
	}

}
function checkInputZip(v,id){
	var email = v.trim();
	var pat=/^[0-9]+$/;
	if(!email.match(pat) || Number(email) == 0 || email.charAt(0)==0 || email.length!=5){
		$('#'+id).val('').focus().notify("Enter correct zip", { className: "error", position:"bottom", });
		return false;
	} else {
		$('.notifyjs-wrapper').trigger('notify-hide');
		return true;
    }
}
function checkInputRCNum(v,id){
	var email = v.trim();
	var pat=/^[0-9]+$/;
	if(!email.match(pat) || Number(email) == 0 || email.charAt(0)==0 || email.length<2){
		$('#'+id).val('').focus().notify("Enter correct ", { className: "error", position:"bottom", });
		return false;
	} else {
		$('.notifyjs-wrapper').trigger('notify-hide');
		return true;
    }
}
function checkInputSelect(v,id){

	if(v=='' || v=='0'){
		$('#'+id).focus().notify("Please select option", { className: "error", position:"bottom", });
		return false;
	} else {
		$('.notifyjs-wrapper').trigger('notify-hide');
		return true;
    }
}

function checkInputUrl(v){
	var email = v.trim();
	//var pat = /(http|ftp|https):\/\/[\w-]+(\.[\w-]+)+([\w.,@?^=%&amp;:\/~+#-]*[\w@?^=%&amp;\/~+#-])?/;
	var pat = new RegExp("^(http[s]?:\\/\\/(www\\.)?|ftp:\\/\\/(www\\.)?|www\\.){1}([0-9A-Za-z-\\.@:%_\+~#=]+)+((\\.[a-zA-Z]{2,3})+)(/(.)*)?(\\?(.)*)?");
	if(email.match(pat)){
		return true;
	} else {
		return false;
    }
}


function checkInputReq(v){
	var email = v.trim();
	if(email!=''){
		return true;
	} else {
		return false;
    }
}


function checkInputNum(v){
	var email = v.trim();
	var pat=/^[0-9]+$/;
	if(!email.match(pat) || Number(email) == 0 || email.charAt(0)==0){
		return false;
	} else {
		return true;
    }
}

function checkgoogleUrl(v){
	var email = v.trim();
	//var pat = /(http|ftp|https):\/\/[\w-]+(\.[\w-]+)+([\w.,@?^=%&amp;:\/~+#-]*[\w@?^=%&amp;\/~+#-])?/;/^(https?:\/\/){0,1}(www\.){0,1}facebook\.com/
	var pat = new RegExp("^(https?:\/\/){0,1}(www\.){0,1}plus\.google\.com(/(.)*)?(\\?(.)*)?");
	if(email.match(pat)){
		return true;
	} else {
		return false;
	}
}
function checkfbUrl(v){
	var email = v.trim();
	//var pat = /(http|ftp|https):\/\/[\w-]+(\.[\w-]+)+([\w.,@?^=%&amp;:\/~+#-]*[\w@?^=%&amp;\/~+#-])?/;/^(https?:\/\/){0,1}(www\.){0,1}facebook\.com/
	var pat = new RegExp("^(https?:\/\/){0,1}(www\.){0,1}facebook\.com(/(.)*)?(\\?(.)*)?");
	if(email.match(pat)){
		return true;
	} else {
		return false;
	}
}
function checktwitterUrl(v){
	var email = v.trim();
	//var pat = /(http|ftp|https):\/\/[\w-]+(\.[\w-]+)+([\w.,@?^=%&amp;:\/~+#-]*[\w@?^=%&amp;\/~+#-])?/;/^(https?:\/\/){0,1}(www\.){0,1}facebook\.com/
	var pat = new RegExp("^(https?:\/\/){0,1}(www\.){0,1}twitter\.com(/(.)*)?(\\?(.)*)?");
	if(email.match(pat)){
		return true;
	} else {
		return false;
	}
}
function checklinkedinUrl(v){
	var email = v.trim();
	//var pat = /(http|ftp|https):\/\/[\w-]+(\.[\w-]+)+([\w.,@?^=%&amp;:\/~+#-]*[\w@?^=%&amp;\/~+#-])?/;/^(https?:\/\/){0,1}(www\.){0,1}facebook\.com/
	var pat = new RegExp("^(https?:\/\/){0,1}(www\.){0,1}linkedin\.com(/(.)*)?(\\?(.)*)?");
	if(email.match(pat)){
		return true;
	} else {
		return false;
	}
}
function checkinstagramUrl(v){
	var email = v.trim();
	//var pat = /(http|ftp|https):\/\/[\w-]+(\.[\w-]+)+([\w.,@?^=%&amp;:\/~+#-]*[\w@?^=%&amp;\/~+#-])?/;/^(https?:\/\/){0,1}(www\.){0,1}facebook\.com/
	var pat = new RegExp("^(https?:\/\/){0,1}(www\.){0,1}instagram\.com(/(.)*)?(\\?(.)*)?");
	if(email.match(pat)){
		return true;
	} else {
		return false;
	}
}
function checkyoutubeUrl(v){
	var email = v.trim();
	//var pat = /(http|ftp|https):\/\/[\w-]+(\.[\w-]+)+([\w.,@?^=%&amp;:\/~+#-]*[\w@?^=%&amp;\/~+#-])?/;/^(https?:\/\/){0,1}(www\.){0,1}facebook\.com/
	var pat = new RegExp("^(https?:\/\/){0,1}(www\.){0,1}youtube\.com(/(.)*)?(\\?(.)*)?");
	if(email.match(pat)){
		return true;
	} else {
		return false;
	}
}


function JSONToCSVConvertor(JSONData, ReportTitle, ShowLabel) {
    //If JSONData is not an object then JSON.parse will parse the JSON string in an Object
                var arrData = typeof JSONData != 'object' ? JSON.parse(JSONData) : JSONData;

                var CSV = '';
                //Set Report title in first row or line

                CSV += ReportTitle + '\r\n\n';

                //This condition will generate the Label/Header
                if (ShowLabel) {
                    var row = "";

                    //This loop will extract the label from 1st index of on array
                    for (var index in arrData[0]) {

                        //Now convert each value to string and comma-seprated
                        row += index + ',';
                    }

                    row = row.slice(0, -1);

                    //append Label row with line break
                    CSV += row + '\r\n';
                }

                //1st loop is to extract each row
                for (var i = 0; i < arrData.length; i++) {
                    var row = "";

                    //2nd loop will extract each column and convert it in string comma-seprated
                    for (var index in arrData[i]) {
                        row += '"' + arrData[i][index] + '",';
                    }

                    row.slice(0, row.length - 1);

                    //add a line break after each row
                    CSV += row + '\r\n';
                }

                if (CSV == '') {
                    alert("Invalid data");
                    return;
                }

                //Generate a file name
                var fileName = "MyReport_";
                //this will remove the blank-spaces from the title and replace it with an underscore
                fileName += ReportTitle.replace(/ /g,"_");

    if(msieversion()){
        setTimeout(function(){
            var IEwindow = window.open('','','toolbar=0,scrollbars=0,location=0,statusbar=0,menubar=0,resizable=0,width=460,height=420');
            IEwindow.document.write('sep=,\r\n' + CSV);
            IEwindow.document.close();
            IEwindow.document.execCommand('SaveAs', true, fileName + ".csv");
            IEwindow.close();
        },2000);

    } else {
        var uri = 'data:application/csv;charset=utf-8,' + escape(CSV);
        var link = document.createElement("a");
        link.href = uri;
        link.style = "visibility:hidden";
        link.download = fileName + ".csv";
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
    }
}


function msieversion() {
    var ua = window.navigator.userAgent;
    var msie = ua.indexOf("MSIE ");
    if (msie != -1 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) // If Internet Explorer, return version number
    {
        return true;
    } else { // If another browser,
        return false;
    }
        return false;
}
