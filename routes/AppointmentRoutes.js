var async = require('async');
var hart= require('../hart');
var url = hart.url;
var session = require('express-session');
var mongoose = require('mongoose');
var multer  = require('multer');
var request = require('request');
var ObjectId = require('mongodb').ObjectID;
var MemoryStore = require('memorystore')(session);
var storage = multer.diskStorage({
  destination: function (req, file, cb) {
      cb(null, hart.bgPath)
  },
  filename: function (req, file, cb) {
      cb(null, Date.now()+'-'+file.originalname)
  }
})

var upload = multer({ storage: storage });
module.exports = function(app){
  app.use(session({
  secret: 'DoctorDx',
  resave: true,
  saveUninitialized: true,
  cookie: { maxAge: 3000 },
  store: new MemoryStore(),
  maxAge: Date.now() + (30 * 86400 * 1000)
  }));
  app.post('/spfloginform',function(req,res){
    if(req.body.userid == "spfstaff"){
      if(req.body.password == "shepherds"){
        res.json("3")
      }else{
        res.json("0")
      }
    }else{
      res.json("2")
    }
  })
  app.post('/appointmentfrompatient',function(req,res){
    var Appointments = hart.appointments;
      var appointments = new Appointments({
        AppointmentId:"DX"+randnum(),
        Name:req.session.Name,
        Email:req.session.Email,
        Mobile:req.body.Mobile,
        Address:req.body.Address,
        AppointmentDate:req.body.AppointmentDate,
        AppointmentTime:req.body.AppointmentTime,
        SubDomain:req.body.SubDomain,
        Status:"1",
      });
      appointments.save(function (err, results) {
        if(err){
          res.json(0);
          console.log(err);
        }
        else{
         
          res.json(1);
        }
      });
  })

  app.post('/EditStudent',function(req,res){
    var id = ObjectId(req.body.id);
    hart.students.updateOne({"_id": id},{ $set:{
        fname : req.body.fname,
        lname : req.body.lname,
        gender : req.body.gender,
        mobilenum : req.body.mobilenum,
        password : "Nill",
        whatsappnumber : req.body.whatsappnumber,
        landlinenumber : req.body.landlinenumber,
        dateofbirth : req.body.dateofbirth,
        RollNumber : req.body.RollNumber,
        medium : req.body.medium,
        courses : req.body.courses,
        qualification : req.body.qualification,
        dateofjoining : req.body.dateofjoining,
        coursefee : req.body.coursefee,
        paidfee : req.body.paidfee,
        address : req.body.address,
        city : req.body.city,
        Street : req.body.Street,
        Village : req.body.Village,
        state : req.body.state,
        zipcode : req.body.zipcode,
     }}, function(err, response) {
        if(!err){
          res.json("1")
        }else{
          res.json("0")
        }
     })
  })
  app.post('/Deletestudent',function(req,res){
    var id = ObjectId(req.body.id);
    hart.students.updateOne({"_id": id},{ $set:{Status : "0",}}, function(err, response) {
        if(!err){
          res.json("1")
        }else{
          res.json("0")
        }
     })
  })
  app.post('/photosedit',upload.any(),function(req,res){
    var id = ObjectId(req.body.id);
    var  eve_pic = req.files[0].filename;
    hart.students.updateOne({"_id": id},{ $set:{Photo : eve_pic,}}, function(err, response) {
        if(!err){
          res.json("1")
        }else{
          res.json("0")
        }
     })
  })
  app.post('/Addstudent',function(req,res){
    var Students = hart.students;
      var studentsdata = new Students({
        StudentId : "SPF"+randnum(),
        fname : req.body.fname,
        lname : req.body.lname,
        gender : req.body.gender,
        mobilenum : req.body.mobilenum,
        password : "Nill",
        whatsappnumber : req.body.whatsappnumber,
        landlinenumber : req.body.landlinenumber,
        dateofbirth : req.body.dateofbirth,
        RollNumber : req.body.RollNumber,
        medium : req.body.medium,
        courses : req.body.courses,
        qualification : req.body.qualification,
        dateofjoining : req.body.dateofjoining,
        coursefee : req.body.coursefee,
        paidfee : req.body.paidfee,
        address : req.body.address,
        city : req.body.city,
        Street : req.body.Street,
        Village : req.body.Village,
        state : req.body.state,
        zipcode : req.body.zipcode,
        BooksSent : "",
        CdSentStatus :  "",
        Photo :   "",
        Certificate :  "",
        Status :  "1",
        optional1 : "",
        optional2 : "",
        optional3 : "",
      });
      studentsdata.save(function (err, results) {
        if(err){
          res.json(0);
          console.log(err);
        }
        else{
         
          res.json(1);
        }
      });
  })

  
  app.post('/Contactform',function(req,res){
    var Contacs = hart.contactus;
      var contactusqueries = new Contacs({
        Name : req.body.name,
        Number : req.body.number,
        Subject : req.body.subject,
        Message : req.body.message,
        Status : "1",
      });
      contactusqueries.save(function (err, results) {
        if(err){
          res.json(0);
          console.log(err);
        }
        else{
        
          res.json(1);
        }
      });
  })
  app.post('/SiteRegister',function(req,res){
    var registrations_site = hart.registrations_site;
      var siteregistrations = new registrations_site({
        StudentId : "SPF"+randnum(),
        fname : req.body.fname,
        lname : req.body.lname,
        mobilenum : req.body.mobilenum,
        courses : req.body.courses,
        Status : "1",
      });
      siteregistrations.save(function (err, results) {
        if(err){
          res.json(0);
          console.log(err);
        }
        else{
      
          res.json(1);
        }
      });
  })
  app.post('/getstudents',function(req,res){
    
      hart.students.find({}).sort({_id:-1}).exec(function(err,doc1){
       if(!err){
         res.json(doc1);
        
       }else{
         res.json("0")
       }
      });
   })
   app.post('/getqueries',function(req,res){
    
    hart.contactus.find({}).sort({_id:-1}).exec(function(err,doc1){
     if(!err){
       res.json(doc1);
     
     }else{
       res.json("0")
     }
    });
 })
 app.post('/getsiteregistrations',function(req,res){
    
  hart.registrations_site.find({}).sort({_id:-1}).exec(function(err,doc1){
   if(!err){
     res.json(doc1);
     
   }else{
     res.json("0")
   }
  });
})
app.post('/getstudentsdetails',function(req,res){
  var id = ObjectId(req.body.id);
  var Query = {"_id": id};
  hart.students.findOne(Query,function(err,data){
   if(!err){
     res.json(data);
     
   }else{
     res.json("0")
   }
  });
})
  function randnum(){
   var chars = "0123456789";
   var string_length = 8;
   var randomstring = '';
   var charCount = 0;
   var numCount = 0;
   for (var i=0; i<string_length; i++) {
       if((Math.floor(Math.random() * 2) == 0) && numCount < 3 || charCount >= 5) {
           var rnum = Math.floor(Math.random() * 10);
           randomstring += rnum;
           numCount += 1;
       } else {
           var rnum = Math.floor(Math.random() * chars.length);
           randomstring += chars.substring(rnum,rnum+1);
           charCount += 1;
       }
   }
   return randomstring;
  }
}
