var path = require('path');
var session = require('express-session');
var hart= require('../hart');
var async = require('async');
var crypto = require('crypto');
var url = require('url');
var mongoose = require('mongoose');
var mongoXlsx = require('mongo-xlsx');
var MongoClient = require('mongodb').MongoClient;
var url = hart.url;
var request = require('request');
var qs = require("querystring");  //For Otp service
var http = require("http");     //For Otp service
var MemoryStore = require('memorystore')(session);
  var auth = function(req,res,next){
  if(req.session && req.session.user != "" && req.session.admin)
  return next();
  else
    return res.redirect('/');
  };


module.exports = function(app){

  app.use(session({
  secret: 'DoctorDx',
  resave: true,
  saveUninitialized: true,
  cookie: { maxAge: 3000 },
  store: new MemoryStore(),
  maxAge: Date.now() + (30 * 86400 * 1000)
  }));


  app.get('/',function(req,res){
    res.sendFile(path.resolve(__dirname,'../Public/index.html'));
  });
  app.get('/Courses',function(req,res){
    res.sendFile(path.resolve(__dirname,'../Public/courses.html'));
  });
  app.get('/Events',function(req,res){
    res.sendFile(path.resolve(__dirname,'../Public/services.html'));
  });
  app.get('/ContactUs',function(req,res){
    res.sendFile(path.resolve(__dirname,'../Public/mail.html'));
  });





  // app.get('/Signup',function(req,res){
  //   res.sendFile(path.resolve(__dirname,'../Public1/sign-up.html'));
  // });
  // app.get('/Staff',function(req,res){
	// 	res.sendFile(path.resolve(__dirname,'../Public1/sign-in.html'));
	// });
  // app.get('/StaffProfile',function(req,res){
	// 	res.sendFile(path.resolve(__dirname,'../Public1/profile.html'));
	// });
  // app.get('/Index1',function(req,res){
	// 	res.sendFile(path.resolve(__dirname,'../Public1/index-2.html'));
	// });
  // app.get('/Registration',function(req,res){
	// 	res.sendFile(path.resolve(__dirname,'../Public1/book-appointment.html'));
	// });
  // app.get('/StudentDetails',function(req,res){
	// 	res.sendFile(path.resolve(__dirname,'../Public1/student-profile.html'));
	// });
  // app.get('/AllStudents',function(req,res){
	// 	res.sendFile(path.resolve(__dirname,'../Public1/students.html'));
	// });
  // app.get('/Students',function(req,res){
	// 	res.sendFile(path.resolve(__dirname,'../Public1/allstudents.html'));
	// });
  // app.get('/StudentRegistration',function(req,res){
	// 	res.sendFile(path.resolve(__dirname,'../Public1/add-student.html'));
	// });
  //
  // app.get('/Payments',function(req,res){
	// 	res.sendFile(path.resolve(__dirname,'../Public1/payments.html'));
	// });
  // app.get('/Addpayment',function(req,res){
	// 	res.sendFile(path.resolve(__dirname,'../Public1/add-payments.html'));
	// });
  // app.get('/Invoices',function(req,res){
	// 	res.sendFile(path.resolve(__dirname,'../Public1/patient-invoice.html'));
	// });

  app.get('/Admin',function(req,res){
		res.sendFile(path.resolve(__dirname,'../Public3/index.html'));
	});
  app.get('/Students',function(req,res){
		res.sendFile(path.resolve(__dirname,'../Public3/agents.html'));
  });
  app.get('/Registrations',function(req,res){
		res.sendFile(path.resolve(__dirname,'../Public3/registrations.html'));
  });
  app.get('/Queries',function(req,res){
		res.sendFile(path.resolve(__dirname,'../Public3/queries.html'));
  });
  app.get('/StudentDetails',function(req,res){
		res.sendFile(path.resolve(__dirname,'../Public3/agent-details.html'));
	});

	// app.get('/ForgetPassword',function(req,res){
	// 	res.sendFile(path.resolve(__dirname,'../Public1/forgot-password.html'));
	// });

 }
